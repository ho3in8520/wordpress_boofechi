<?php

return array(
	'id'     => 'roofplan_banner_settings',
	'title'  => esc_html__( "تنظیمات بنر روف پلن", "konia" ),
	'fields' => array(
		array(
			'id'      => 'banner_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سورس بنر', 'roofplan' ),
			'options' => array(
				'd' => esc_html__( 'پیشفرض', 'roofplan' ),
				'e' => esc_html__( 'المنتور', 'roofplan' ),
			),
			'default' => '',
		),
		array(
			'id'       => 'banner_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'viral-buzz' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
				'posts_per_page'=> -1,
			],
			'required' => [ 'banner_source_type', '=', 'e' ],
		),
		array(
			'id'       => 'banner_page_banner',
			'type'     => 'switch',
			'title'    => esc_html__( 'نمایش بنر', 'roofplan' ),
			'default'  => false,
			'required' => [ 'banner_source_type', '=', 'd' ],
		),
		array(
			'id'       => 'banner_banner_title',
			'type'     => 'text',
			'title'    => esc_html__( 'عنوان بخش بنر', 'roofplan' ),
			'desc'     => esc_html__( '', 'roofplan' ),
			'required' => array( 'banner_page_banner', '=', true ),
		),
		array(
			'id'       => 'banner_page_background',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'تصویر پس زمینه', 'roofplan' ),
			'desc'     => esc_html__( '', 'roofplan' ),
			'default'  => array(
				'url' => ROOFPLAN_URI . 'assets/images/breadcrumb/breadcrumb-1.jpg',
			),
			'required' => array( 'banner_page_banner', '=', true ),
		),
	),
);