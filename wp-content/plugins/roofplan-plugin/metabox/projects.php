<?php
return array(
	'title'      => 'تنظیمات پروژه روف پلن',
	'id'         => 'roofplan_meta_projects',
	'icon'       => 'el el-cogs',
	'position'   => 'normal',
	'priority'   => 'core',
	'post_types' => array( 'roofplan_project' ),
	'sections'   => array(
		array(
			'id'     => 'roofplan_projects_meta_setting',
			'fields' => array(
				array(
					'id'    => 'meta_subtitle',
					'type'  => 'text',
					'title' => esc_html__( 'زیر عنوان', 'roofplan' ),
				),
				array(
					'id'    => 'page_link',
					'type'  => 'text',
					'title' => esc_html__( 'لینک صفحه', 'roofplan' ),
				),
				array(
					'id'    => 'image_link',
					'type'  => 'text',
					'title' => esc_html__( 'لینک تصویر', 'roofplan' ),
				),
				array(
					'id'    => 'meta_number',
					'type'  => 'text',
					'title' => esc_html__( 'ستون', 'roofplan' ),
					'default' => esc_html__( '3', 'roofplan' ),
				),
			),
		),
	),
);