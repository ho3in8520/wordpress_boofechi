<?php
return array(
	'title' => 'تنظیمات پست روف پلن',
	'id' => 'roofplan_meta_post',
	'icon' => 'el el-cogs',
	'position' => 'normal',
	'priority' => 'core',
	'post_types' => array( 'post' ),
	'sections' => array(
		array(
			'id' => 'roofplan_post_meta_setting',
			'fields' => array(
				array(
					'id' => 'meta_image',
					'type' => 'media',
					'title' => esc_html__( 'متا تصویر', 'indext' ),
				),
				

			),
		),
	),
);