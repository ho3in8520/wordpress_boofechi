<?php
return array(
	'title'      => 'تنظیمات خدمات روف پلن',
	'id'         => 'roofplan_meta_service',
	'icon'       => 'el el-cogs',
	'position'   => 'normal',
	'priority'   => 'core',
	'post_types' => array( 'roofplan_service' ),
	'sections'   => array(
		array(
			'id'     => 'roofplan_service_meta_setting',
			'fields' => array(
				array(
					'id'       => 'service_icon',
					'type'     => 'select',
					'title'    => esc_html__( 'آیکون خدمات', 'roofplan' ),
					'options'  => get_fontawesome_icons(),
				),
				array(
					'id'    => 'ext_url',
					'type'  => 'text',
					'title' => esc_html__( 'لینک مشاهده بیشتر', 'roofplan' ),
				),
			),
		),
	),
);