<?php

return array(
	'id'     => 'roofplan_header_settings',
	'title'  => esc_html__( "تنظیمات سربرگ روف پلن", "konia" ),
	'fields' => array(
		array(
			'id'      => 'header_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سورس سربرگ', 'roofplan' ),
			'options' => array(
				'd' => esc_html__( 'پیش فرض', 'roofplan' ),
				'e' => esc_html__( 'المنتور', 'roofplan' ),
			),
			'default'=> '',
		),
		array(
			'id'       => 'header_new_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'roofplan-plugin' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
				'posts_per_page' => -1,
				'orderby'  => 'title',
				'order'     => 'DESC'
			],
			'required' => [ 'header_source_type', '=', 'e' ],
		),
		array(
			'id'       => 'header_style_settings',
			'type'     => 'image_select',
			'title'    => esc_html__( 'انتخاب استایل سربرگ', 'roofplan' ),
			'options'  => array(
				'header_v1' => array(
					'alt' => 'هدر استایل یک',
					'img' => get_template_directory_uri() . '/assets/images/redux/header/header1.png',
				),
				'header_v2' => array(
					'alt' => 'هدر استایل دو',
					'img' => get_template_directory_uri() . '/assets/images/redux/header/header2.png',
				),
				
				'header_v3' => array(
					'alt' => 'هدر استایل سه',
					'img' => get_template_directory_uri() . '/assets/images/redux/header/header3.png',
				),
				'header_v4' => array(
					'alt' => 'هدر استایل چهار',
					'img' => get_template_directory_uri() . '/assets/images/redux/header/onepage.png',
				),
				'header_v5' => array(
					'alt' => 'هدر استایل پنج',
					'img' => get_template_directory_uri() . '/assets/images/redux/header/header5.png',
				),
			),
			'required' => array( array( 'header_source_type', 'equals', 'd' ) ),
		),
	),
);