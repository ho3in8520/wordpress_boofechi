<?php
return array(
	'title'      => 'تنظیمات تیم روف پلن',
	'id'         => 'roofplan_meta_team',
	'icon'       => 'el el-cogs',
	'position'   => 'normal',
	'priority'   => 'core',
	'post_types' => array( 'roofplan_team' ),
	'sections'   => array(
		array(
			'id'     => 'roofplan_team_meta_setting',
			'fields' => array(
				array(
					'id'    => 'designation',
					'type'  => 'text',
					'title' => esc_html__( 'نقش', 'roofplan' ),
				),
				array(
					'id'    => 'team_link',
					'type'  => 'text',
					'title' => esc_html__( 'لینک مشاهده بیشتر', 'roofplan' ),
				),
				array(
					'id'    => 'social_profile',
					'type'  => 'social_media',
					'title' => esc_html__( 'لینک نمونه کار', 'roofplan' ),
				),
			),
		),
	),
);