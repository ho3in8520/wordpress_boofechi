<?php
return array(
	'title'      => 'تنظیمات رویداد روف پلن',
	'id'         => 'roofplan_meta_event',
	'icon'       => 'el el-cogs',
	'position'   => 'normal',
	'priority'   => 'core',
	'post_types' => array( 'tribe_events' ),
	'sections'   => array(
		array(
			'id'     => 'roofplan_event_meta_setting',
			'fields' => array(
				array(
					'id'    => 'video',
					'type'  => 'text',
					'title' => esc_html__( 'لینک ویدئو', 'roofplan' ),
				),
				
			),
		),
	),
);