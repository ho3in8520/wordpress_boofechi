<?php

return array(
	'id'     => 'roofplan_sidebar_settings',
	'title'  => esc_html__( "تنظیمات سایدبار روف پلن", "konia" ),
	'fields' => array(
		array(
			'id'      => 'sidebar_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سایدبار', 'roofplan' ),
			'options' => array(
				'd' => esc_html__( 'پیش فرض', 'roofplan' ),
				'e' => esc_html__( 'المنتور', 'roofplan' ),
			),
			'default'=> '',
		),
		array(
			'id'       => 'sidebar_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'viral-buzz' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
				'posts_per_page'=> -1,
			],
			'required' => [ 'sidebar_source_type', '=', 'e' ],
		),
		array(
			'id'       => 'sidebar_sidebar_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'لایه', 'roofplan' ),
			'subtitle' => esc_html__( 'محتوای اصلی و تراز نوار کناری را انتخاب کنید.', 'roofplan' ),
			'options'  => array(
				'left'  => array(
					'alt' => esc_html__( '2 ستونه چپ', 'roofplan' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cl.png',
				),
				'full'  => array(
					'alt' => esc_html__( '1 ستونه', 'roofplan' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/1col.png',
				),
				'right' => array(
					'alt' => esc_html__( '2 ستونه راست', 'roofplan' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cr.png',
				),
			),
			'required' => [ 'sidebar_source_type', '=', 'd' ],
		),

		array(
			'id'       => 'sidebar_page_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'سایدبار', 'roofplan' ),
			'required' => array(
				array( 'sidebar_sidebar_layout', '=', array( 'left', 'right' ) ),
			),
			'options'  => roofplans_get_sidebars(),
			'required' => [ 'sidebar_source_type', '=', 'd' ],
		),
	),
);