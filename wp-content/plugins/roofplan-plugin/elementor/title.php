<?php

namespace ROOFPLANPLUGIN\Element;

use Elementor\Controls_Manager;
use Elementor\Controls_Stack;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Repeater;
use Elementor\Widget_Base;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Plugin;
use Elementor\Utils;

/**
 * Elementor button widget.
 * Elementor widget that displays a button with the ability to control every
 * aspect of the button design.
 *
 * @since 1.0.0
 */
class Title extends Widget_Base {

	/**
	 * Get widget name.
	 * Retrieve button widget name.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'roofplan_title';
	}

	/**
	 * Get widget title.
	 * Retrieve button widget title.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Title', 'roofplan' );
	}

	/**
	 * Get widget icon.
	 * Retrieve button widget icon.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-briefcase';
	}

	/**
	 * Get widget categories.
	 * Retrieve the list of categories the button widget belongs to.
	 * Used to determine where to display the widget in the editor.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'roofplan' ];
	}
	
	/**
	 * Register button widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since  1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'h1_blog',
			[
				'label' => esc_html__( 'Title', 'roofplan' ),
			]
		);
		$this->add_control(
			'sec_class',
			[
				'label'       => __( 'Section Class', 'rashid' ),
				'type'        => Controls_Manager::TEXTAREA,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Enter Section Class', 'rashid' ),
			]
		);
		$this->add_control(
			'subtitle',
			[
				'label'       => __( 'Sub Title', 'rashid' ),
				'type'        => Controls_Manager::TEXTAREA,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Enter your Sub title', 'rashid' ),
			]
		);
		$this->add_control(
			'title',
			[
				'label'       => __( 'Title', 'rashid' ),
				'type'        => Controls_Manager::TEXTAREA,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Enter your title', 'rashid' ),
			]
		);
		$this->add_control(
			'alingment',
			[
				'label'   => esc_html__( 'Alignment', 'rashid' ),
				'type'    => Controls_Manager::SELECT,
				'default' => __( 'text-center', 'rashid' ),
				'options' => array(
					'text-left'   => esc_html__( 'Aling Left', 'rashid' ),
					'text-center'   => esc_html__( 'Align Center', 'rashid' ),
				),
			]
		);
		$this->add_control(
			'rashid_version',
			[
				'label'   => esc_html__( 'Dark/Light', 'rashid' ),
				'type'    => Controls_Manager::SELECT,
				'default' => __( 'rashid_light', 'rashid' ),
				'options' => array(
					'rashid_light'   => esc_html__( 'Light Version', 'rashid' ),
					'rashid_dark'   => esc_html__( 'Dark Version', 'rashid' ),
				),
			]
		);
		
		
		
		$this->end_controls_section();
	
	}

	/**
	 * Render button widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since  1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
        
        $paged = roofplan_set($_POST, 'paged') ? esc_attr($_POST['paged']) : 1;

		$this->add_render_attribute( 'wrapper', 'class', 'templatepath-roofplan' );
		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => roofplan_set( $settings, 'query_number' ),
			'orderby'        => roofplan_set( $settings, 'query_orderby' ),
			'order'          => roofplan_set( $settings, 'query_order' ),
			'paged'         => $paged
		);
		if ( roofplan_set( $settings, 'query_exclude' ) ) {
			$settings['query_exclude'] = explode( ',', $settings['query_exclude'] );
			$args['post__not_in']      = roofplan_set( $settings, 'query_exclude' );
		}
		if( roofplan_set( $settings, 'query_category' ) ) $args['category_name'] = roofplan_set( $settings, 'query_category' );
		$query = new \WP_Query( $args );

		if ( $query->have_posts() ) 
		{ ?>

		<!--Start Blog Style1 Area-->
		<section class=" section_title <?php echo esc_attr($settings['sec_class']);?> <?php echo esc_attr($settings['rashid_version']);?>">
			<div class="container">
				<?php if($settings['title']): ?>
				<div class="sec-title  <?php echo esc_attr($settings['alingment']);?>">
					<div class="sub-title">
						<p><?php echo $settings['subtitle'];?></p>
					</div>
					<h2><?php echo $settings['title'];?></h2>
				</div>
				<?php endif; ?>
			</div>
		</section>
		<!--End Blog Style1 Area-->
		
        <?php }
		wp_reset_postdata();
	}

}