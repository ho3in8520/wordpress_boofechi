<?php

namespace ROOFPLANPLUGIN\Element;

use Elementor\Controls_Manager;
use Elementor\Controls_Stack;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Repeater;
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Plugin;

/**
 * Elementor button widget.
 * Elementor widget that displays a button with the ability to control every
 * aspect of the button design.
 *
 * @since 1.0.0
 */
class H2_Slider extends Widget_Base {

	/**
	 * Get widget name.
	 * Retrieve button widget name.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'roofplan_h2_slider';
	}

	/**
	 * Get widget title.
	 * Retrieve button widget title.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'H2 Slider', 'roofplan' );
	}

	/**
	 * Get widget icon.
	 * Retrieve button widget icon.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-briefcase';
	}

	/**
	 * Get widget categories.
	 * Retrieve the list of categories the button widget belongs to.
	 * Used to determine where to display the widget in the editor.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'roofplan' ];
	}
	
	/**
	 * Register button widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since  1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'h2_slider',
			[
				'label' => esc_html__( 'H2 Slider', 'roofplan' ),
			]
		);
		$this->add_control(
			'sec_class',
			[
				'label'       => __( 'Section Class', 'rashid' ),
				'type'        => Controls_Manager::TEXTAREA,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Enter Section Class', 'rashid' ),
			]
		);
		
		
		$this->end_controls_section();
		
		// New Tab#1

		$this->start_controls_section(
					'content_section',
					[
						'label' => __( 'Slider Block', 'rashid' ),
						'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
					]
				);
				$this->add_control(
				  'repeat', 
					[
						'type' => Controls_Manager::REPEATER,
						'seperator' => 'before',
						'default' => 
							[
								['block_title' => esc_html__('Projects Completed', 'rashid')],
							],
						'fields' => 
							[
								
								[
									'name' => 'block_image',
									'label' => __( 'Image', 'rashid' ),
									'type' => Controls_Manager::MEDIA,
									'default' => ['url' => Utils::get_placeholder_image_src(),],
								],
								[
									'name' => 'block_alt_text',
									'label' => esc_html__('Alt Text', 'rashid'),
									'type' => Controls_Manager::TEXTAREA,
									'default' => esc_html__('', 'rashid')
								],
								[
									'name' => 'block_image2',
									'label' => __( 'Image', 'rashid' ),
									'type' => Controls_Manager::MEDIA,
									'default' => ['url' => Utils::get_placeholder_image_src(),],
								],
								[
									'name' => 'block_alt_text2',
									'label' => esc_html__('Alt Text', 'rashid'),
									'type' => Controls_Manager::TEXTAREA,
									'default' => esc_html__('', 'rashid')
								],
								[
									'name' => 'block_image3',
									'label' => __( 'Image', 'rashid' ),
									'type' => Controls_Manager::MEDIA,
									'default' => ['url' => Utils::get_placeholder_image_src(),],
								],
								[
									'name' => 'block_alt_text3',
									'label' => esc_html__('Alt Text', 'rashid'),
									'type' => Controls_Manager::TEXTAREA,
									'default' => esc_html__('', 'rashid')
								],
								[
									'name' => 'block_image4',
									'label' => __( 'Image', 'rashid' ),
									'type' => Controls_Manager::MEDIA,
									'default' => ['url' => Utils::get_placeholder_image_src(),],
								],
								[
									'name' => 'block_alt_text4',
									'label' => esc_html__('Alt Text', 'rashid'),
									'type' => Controls_Manager::TEXTAREA,
									'default' => esc_html__('', 'rashid')
								],
								
								[
									'name' => 'block_title',
									'label' => esc_html__('Title', 'rashid'),
									'type' => Controls_Manager::TEXTAREA,
									'default' => esc_html__('', 'rashid')
								],
								[
									'name' => 'block_text',
									'label' => esc_html__('Text', 'rashid'),
									'type' => Controls_Manager::TEXTAREA,
									'default' => esc_html__('', 'rashid')
								],
								[
									'name' => 'block_button',
									'label'       => __( 'Button', 'rashid' ),
									'type'        => Controls_Manager::TEXT,
									'dynamic'     => [
										'active' => true,
									],
									'placeholder' => __( 'Enter your Button Title', 'rashid' ),
								],
								[
									'name' => 'block_btnlink',
									'label' => __( 'Button Url', 'rashid' ),
									'type' => Controls_Manager::URL,
									'placeholder' => __( 'https://your-link.com', 'rashid' ),
									'show_external' => true,
									'default' => [
									'url' => '',
									'is_external' => true,
									'nofollow' => true,
									],
								],
								[
									'name' => 'block_button2',
									'label'       => __( 'Button', 'rashid' ),
									'type'        => Controls_Manager::TEXT,
									'dynamic'     => [
										'active' => true,
									],
									'placeholder' => __( 'Enter your Button Title', 'rashid' ),
								],
								[
									'name' => 'block_btnlink2',
									'label' => __( 'Button Url', 'rashid' ),
									'type' => Controls_Manager::URL,
									'placeholder' => __( 'https://your-link.com', 'rashid' ),
									'show_external' => true,
									'default' => [
									'url' => '',
									'is_external' => true,
									'nofollow' => true,
									],
								],
								[
									'name' => 'block_bgimg',
									'label' => esc_html__('Background image', 'rashid'),
									'type' => Controls_Manager::MEDIA,
									'default' => ['url' => Utils::get_placeholder_image_src(),],
								],
								[
									'name' => 'block_title2',
									'label' => esc_html__('Title', 'rashid'),
									'type' => Controls_Manager::TEXTAREA,
									'default' => esc_html__('', 'rashid')
								],
								[
									'name' => 'block_btnlink3',
									'label' => __( 'Video Link', 'rashid' ),
									'type' => Controls_Manager::URL,
									'placeholder' => __( 'https://your-link.com', 'rashid' ),
									'show_external' => true,
									'default' => [
									'url' => '',
									'is_external' => true,
									'nofollow' => true,
									],
								],
							],
						'title_field' => '{{block_title}}',
					 ]
			);
				
				
		$this->end_controls_section();	
		
	
		}

	/**
	 * Render button widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since  1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		$allowed_tags = wp_kses_allowed_html('post');
		?>
        
		<section class="main-slider style2 <?php echo esc_attr($settings['sec_class']);?>">
			<div class="slider-box">
				<!-- Banner Carousel -->
				<div class="banner-carousel owl-theme owl-carousel">

					<?php foreach($settings['repeat'] as $item):?>
					<div class="slide">
						<div class="shape1 wow slideInLeft" data-wow-delay="100ms" data-wow-duration="1000ms">
							<?php if(wp_get_attachment_url($item['block_image']['id'])): ?>
							<img class="float-bob-y" src="<?php echo wp_get_attachment_url($item['block_image']['id']);?>" alt="<?php echo wp_kses($item['block_alt_text'], $allowed_tags);?>">
							<?php else :?>
							<div class="noimage"></div>
							<?php endif;?>
						</div>
						<div class="shape2 wow slideInDown" data-wow-delay="100ms" data-wow-duration="1000ms">
							<?php if(wp_get_attachment_url($item['block_image2']['id'])): ?>
							<img class="float-bob-x" src="<?php echo wp_get_attachment_url($item['block_image2']['id']);?>" alt="<?php echo wp_kses($item['block_alt_text2'], $allowed_tags);?>">
							<?php else :?>
							<div class="noimage"></div>
							<?php endif;?>
						</div>
						<div class="shape3 wow slideInLeft" data-wow-delay="100ms" data-wow-duration="1000ms">
							<?php if(wp_get_attachment_url($item['block_image3']['id'])): ?>
							<img class="rotate-me" src="<?php echo wp_get_attachment_url($item['block_image3']['id']);?>" alt="<?php echo wp_kses($item['block_alt_text3'], $allowed_tags);?>">
							<?php else :?>
							<div class="noimage"></div>
							<?php endif;?>
						</div>
						<div class="shape4 wow zoomIn" data-wow-duration="1000ms">
							<?php if(wp_get_attachment_url($item['block_image4']['id'])): ?>
							<img class="zoom-fade" src="<?php echo wp_get_attachment_url($item['block_image4']['id']);?>" alt="<?php echo wp_kses($item['block_alt_text4'], $allowed_tags);?>">
							<?php else :?>
							<div class="noimage"></div>
							<?php endif;?>
						</div>
						<div class="auto-container">
							<div class="content">
								<div class="big-title">
									<h2><?php echo wp_kses($item['block_title'], $allowed_tags);?></h2>
								</div>
								<div class="text">
									<p><?php echo wp_kses($item['block_text'], $allowed_tags);?></p>
								</div>
								<div class="btns-box">
									<?php if(wp_kses($item['block_button'], $allowed_tags)): ?>
									<a class="btn-one" href="<?php echo esc_url($item['block_btnlink']['url']);?>">
										<span class="txt"><?php echo wp_kses($item['block_button'], $allowed_tags);?></span>
									</a>
									<?php endif; ?>
									<?php if(wp_kses($item['block_button2'], $allowed_tags)): ?>
									<a class="btn-one style2" href="<?php echo esc_url($item['block_btnlink2']['url']);?>">
										<span class="txt"><?php echo wp_kses($item['block_button2'], $allowed_tags);?></span>
									</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="slide-right-box">
								<?php if(wp_get_attachment_url($item['block_bgimg']['id'])): ?>
								<div class="slider-right-box-bg" style="background-image: url(<?php echo wp_get_attachment_url($item['block_bgimg']['id']);?>);">
								<?php else :?>
								<div class="noimage">
								<?php endif;?>
								</div>
								<div class="video-holder-box1">
									<div class="icon wow zoomIn" data-wow-delay="300ms" data-wow-duration="1500ms">
										<a class="video-popup" title="Video Gallery" href="<?php echo esc_url($item['block_btnlink3']['url']);?>">
											<span class="flaticon-play-button"></span>
										</a>
									</div>
									<div class="title">
										<h4><?php echo wp_kses($item['block_title2'], $allowed_tags);?></h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</section>
             
		<?php 
	}

}