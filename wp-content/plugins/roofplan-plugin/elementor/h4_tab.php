<?php

namespace ROOFPLANPLUGIN\Element;

use Elementor\Controls_Manager;
use Elementor\Controls_Stack;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Repeater;
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Plugin;

/**
 * Elementor button widget.
 * Elementor widget that displays a button with the ability to control every
 * aspect of the button design.
 *
 * @since 1.0.0
 */
class H4_Tab extends Widget_Base {

	/**
	 * Get widget name.
	 * Retrieve button widget name.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'roofplan_h4_tab';
	}

	/**
	 * Get widget title.
	 * Retrieve button widget title.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'H4 Tab', 'roofplan' );
	}

	/**
	 * Get widget icon.
	 * Retrieve button widget icon.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-briefcase';
	}

	/**
	 * Get widget categories.
	 * Retrieve the list of categories the button widget belongs to.
	 * Used to determine where to display the widget in the editor.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'roofplan' ];
	}
	
	/**
	 * Register button widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since  1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'h4_tab',
			[
				'label' => esc_html__( 'H4 Tab', 'roofplan' ),
			]
		);
		$this->add_control(
			'sec_class',
			[
				'label'       => __( 'Section Class', 'rashid' ),
				'type'        => Controls_Manager::TEXTAREA,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Enter Section Class', 'rashid' ),
			]
		);
		$this->add_control(
			'rashid_version',
			[
				'label'   => esc_html__( 'Dark/Light', 'rashid' ),
				'type'    => Controls_Manager::SELECT,
				'default' => __( 'rashid_light', 'rashid' ),
				'options' => array(
					'rashid_light'   => esc_html__( 'Light Version', 'rashid' ),
					'rashid_dark'   => esc_html__( 'Dark Version', 'rashid' ),
				),
			]
		);
		
		$this->add_control(
			'bgimg',
			[
				'label' => esc_html__('Background image', 'rashid'),
				'type' => Controls_Manager::MEDIA,
				'default' => ['url' => Utils::get_placeholder_image_src(),],
			]
		);
		$this->add_control(
			'bgimg2',
			[
				'label' => esc_html__('Background image', 'rashid'),
				'type' => Controls_Manager::MEDIA,
				'default' => ['url' => Utils::get_placeholder_image_src(),],
			]
		);
		$this->add_control(
			'subtitle',
			[
				'label'       => __( 'Sub Title', 'rashid' ),
				'type'        => Controls_Manager::TEXTAREA,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Enter your Sub title', 'rashid' ),
			]
		);
		$this->add_control(
			'title',
			[
				'label'       => __( 'Title', 'rashid' ),
				'type'        => Controls_Manager::TEXTAREA,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Enter your title', 'rashid' ),
			]
		);
		
		
		$this->end_controls_section();
		
		// New Tab#1

		$this->start_controls_section(
					'content_section',
					[
						'label' => __( 'Tab Block One', 'rashid' ),
						'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
					]
				);
				$this->add_control(
					'bgimg3',
					[
						'label' => esc_html__('Background image', 'rashid'),
						'type' => Controls_Manager::MEDIA,
						'default' => ['url' => Utils::get_placeholder_image_src(),],
					]
				);
				$this->add_control(
					'image',
						[
						  'label' => __( 'Image', 'rashid' ),
						  'type' => Controls_Manager::MEDIA,
						  'default' => ['url' => Utils::get_placeholder_image_src(),],
						]
				);
				$this->add_control(
					'alt_text',
					[
						'label'       => __( 'Alt text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'image2',
						[
						  'label' => __( 'Image', 'rashid' ),
						  'type' => Controls_Manager::MEDIA,
						  'default' => ['url' => Utils::get_placeholder_image_src(),],
						]
				);
				$this->add_control(
					'alt_text2',
					[
						'label'       => __( 'Alt text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_one_title',
					[
						'label'       => __( 'Title', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your title', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_one_text',
					[
						'label'       => __( 'Description Text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_one_text2',
					[
						'label'       => __( 'Description Text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_one_title2',
					[
						'label'       => __( 'Title', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your title', 'rashid' ),
					]
				);
				$this->add_control(
				  'repeat', 
					[
						'type' => Controls_Manager::REPEATER,
						'seperator' => 'before',
						'default' => 
							[
								['block_title' => esc_html__('Projects Completed', 'rashid')],
							],
						'fields' => 
							[
								[
									'name' => 'block_title',
									'label' => esc_html__('Title', 'rashid'),
									'type' => Controls_Manager::TEXTAREA,
									'default' => esc_html__('', 'rashid')
								],
							],
						'title_field' => '{{block_title}}',
					 ]
				);
				
				
		$this->end_controls_section();	
		
		// New Tab#2

		$this->start_controls_section(
					'content_section2',
					[
						'label' => __( 'Tab Block Two', 'rashid' ),
						'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
					]
				);
				$this->add_control(
					'tab_two_title',
					[
						'label'       => __( 'Title', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your title', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_two_text',
					[
						'label'       => __( 'Description Text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_two_text2',
					[
						'label'       => __( 'Description Text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'bgimg4',
					[
						'label' => esc_html__('Background image', 'rashid'),
						'type' => Controls_Manager::MEDIA,
						'default' => ['url' => Utils::get_placeholder_image_src(),],
					]
				);
				$this->add_control(
					'image3',
						[
						  'label' => __( 'Image', 'rashid' ),
						  'type' => Controls_Manager::MEDIA,
						  'default' => ['url' => Utils::get_placeholder_image_src(),],
						]
				);
				$this->add_control(
					'alt_text3',
					[
						'label'       => __( 'Alt text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
				  'repeat2', 
					[
						'type' => Controls_Manager::REPEATER,
						'seperator' => 'before',
						'default' => 
							[
								['block_title' => esc_html__('Projects Completed', 'rashid')],
							],
						'fields' => 
							[
								[
									'name' => 'block_title',
									'label' => esc_html__('Title', 'rashid'),
									'type' => Controls_Manager::TEXTAREA,
									'default' => esc_html__('', 'rashid')
								],
							],
						'title_field' => '{{block_title}}',
					 ]
				);
				
		$this->end_controls_section();	
		
		// New Tab#3

		$this->start_controls_section(
					'content_section3',
					[
						'label' => __( 'Tab Block Three', 'rashid' ),
						'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
					]
				);
				$this->add_control(
					'tab_three_title',
					[
						'label'       => __( 'Title', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your title', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_three_text',
					[
						'label'       => __( 'Description Text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_three_title2',
					[
						'label'       => __( 'Title', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your title', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_three_text2',
					[
						'label'       => __( 'Description Text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_three_text3',
					[
						'label'       => __( 'Description Text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'bgimg5',
					[
						'label' => esc_html__('Background image', 'rashid'),
						'type' => Controls_Manager::MEDIA,
						'default' => ['url' => Utils::get_placeholder_image_src(),],
					]
				);
				$this->add_control(
					'image4',
						[
						  'label' => __( 'Image', 'rashid' ),
						  'type' => Controls_Manager::MEDIA,
						  'default' => ['url' => Utils::get_placeholder_image_src(),],
						]
				);
				$this->add_control(
					'alt_text4',
					[
						'label'       => __( 'Alt text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'image5',
						[
						  'label' => __( 'Image', 'rashid' ),
						  'type' => Controls_Manager::MEDIA,
						  'default' => ['url' => Utils::get_placeholder_image_src(),],
						]
				);
				$this->add_control(
					'alt_text5',
					[
						'label'       => __( 'Alt text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'image6',
						[
						  'label' => __( 'Image', 'rashid' ),
						  'type' => Controls_Manager::MEDIA,
						  'default' => ['url' => Utils::get_placeholder_image_src(),],
						]
				);
				$this->add_control(
					'alt_text6',
					[
						'label'       => __( 'Alt text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				
				
		$this->end_controls_section();	
		
		// New Tab#4

		$this->start_controls_section(
					'content_section4',
					[
						'label' => __( 'Tab Block Four', 'rashid' ),
						'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
					]
				);
				$this->add_control(
					'bgimg6',
					[
						'label' => esc_html__('Background image', 'rashid'),
						'type' => Controls_Manager::MEDIA,
						'default' => ['url' => Utils::get_placeholder_image_src(),],
					]
				);
				$this->add_control(
					'image7',
						[
						  'label' => __( 'Image', 'rashid' ),
						  'type' => Controls_Manager::MEDIA,
						  'default' => ['url' => Utils::get_placeholder_image_src(),],
						]
				);
				$this->add_control(
					'alt_text7',
					[
						'label'       => __( 'Alt text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'image8',
						[
						  'label' => __( 'Image', 'rashid' ),
						  'type' => Controls_Manager::MEDIA,
						  'default' => ['url' => Utils::get_placeholder_image_src(),],
						]
				);
				$this->add_control(
					'alt_text8',
					[
						'label'       => __( 'Alt text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_four_title',
					[
						'label'       => __( 'Title', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your title', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_four_text',
					[
						'label'       => __( 'Description Text', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Description', 'rashid' ),
					]
				);
				$this->add_control(
					'tab_four_title2',
					[
						'label'       => __( 'Title', 'rashid' ),
						'type'        => Controls_Manager::TEXTAREA,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your title', 'rashid' ),
					]
				);
				$this->add_control(
					'bttn',
					[
						'label'       => __( 'Button', 'rashid' ),
						'type'        => Controls_Manager::TEXT,
						'dynamic'     => [
							'active' => true,
						],
						'placeholder' => __( 'Enter your Button Title', 'rashid' ),
					]
				);	
				$this->add_control(
					'btnlink',
					[
					  'label' => __( 'Button Url', 'rashid' ),
					  'type' => Controls_Manager::URL,
					  'placeholder' => __( 'https://your-link.com', 'rashid' ),
					  'show_external' => true,
					  'default' => [
						'url' => '',
						'is_external' => true,
						'nofollow' => true,
					  ],
					
				   ]
				);
				$this->add_control(
				  'repeat3', 
					[
						'type' => Controls_Manager::REPEATER,
						'seperator' => 'before',
						'default' => 
							[
								['block_title' => esc_html__('Projects Completed', 'rashid')],
							],
						'fields' => 
							[
								[
									'name' => 'block_title',
									'label' => esc_html__('Title', 'rashid'),
									'type' => Controls_Manager::TEXTAREA,
									'default' => esc_html__('', 'rashid')
								],
							],
						'title_field' => '{{block_title}}',
					 ]
				);
				
				
		$this->end_controls_section();	
		
	
		}

	/**
	 * Render button widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since  1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		$allowed_tags = wp_kses_allowed_html('post');
		?>
        
		<section class="service-home-four <?php echo esc_attr($settings['sec_class']);?> <?php echo esc_attr($settings['rashid_version']);?>">
			<div class="pattern-layer">
				<?php  if ( esc_url($settings['bgimg']['id']) ) : ?>
				<div class="pattern-1" style="background-image: url(<?php echo wp_get_attachment_url($settings['bgimg']['id']);?>);">
				<?php else :?>	
				<div class="noimage">
				<?php endif;?>
				</div>
				<?php  if ( esc_url($settings['bgimg2']['id']) ) : ?>
				<div class="pattern-2" style="background-image: url(<?php echo wp_get_attachment_url($settings['bgimg2']['id']);?>);">
				<?php else :?>	
				<div class="noimage">
				<?php endif;?>
				</div>
			</div>
			<div class="container">
				<?php if($settings['title']): ?>
				<div class="sec-title text-center">
					<div class="sub-title">
						<p><?php echo $settings['subtitle'];?></p>
					</div>
					<h2><?php echo $settings['title'];?></h2>
				</div>
				<?php endif; ?>
				<div class="tabs-box">
					<div class="tab-btn-box text-center">
						<div class="tab-btns tab-buttons clearfix">
							<div class="row clearfix">
								<div class="col-lg-3 col-md-6 col-sm-12 single-btn">
									<div class="tab-btn active-btn" data-tab="#tab-1"><h4><?php echo $settings['tab_one_title'];?></h4></div>
								</div>
								<div class="col-lg-3 col-md-6 col-sm-12 single-btn">
									<div class="tab-btn" data-tab="#tab-2"><h4><?php echo $settings['tab_two_title'];?></h4></div>
								</div>
								<div class="col-lg-3 col-md-6 col-sm-12 single-btn">
									<div class="tab-btn" data-tab="#tab-3"><h4><?php echo $settings['tab_three_title'];?></h4></div>
								</div>
								<div class="col-lg-3 col-md-6 col-sm-12 single-btn">
									<div class="tab-btn" data-tab="#tab-4"><h4><?php echo $settings['tab_four_title'];?></h4></div>
								</div>
							</div>
						</div>
					</div>
					<div class="tabs-content">
						<div class="tab active-tab" id="tab-1">
							<div class="content-inner">
								<div class="row clearfix">
									<div class="col-lg-6 col-md-12 col-sm-12 image-column">
										<div class="image-box">
											<?php  if ( esc_url($settings['bgimg3']['id']) ) : ?>
											<div class="image-shape-1 rotate-me" style="background-image: url(<?php echo wp_get_attachment_url($settings['bgimg3']['id']);?>);">
											<?php else :?>	
											<div class="noimage">
											<?php endif;?>
											</div>
											<figure class="image image-1">
												<?php  if ( esc_url($settings['image']['id']) ) : ?>   
												<img src="<?php echo wp_get_attachment_url($settings['image']['id']);?>" alt="<?php echo esc_attr($settings['alt_text']);?>"/>
												<?php else :?>
												<div class="noimage"></div>
												<?php endif;?>
											</figure>
											
											<figure class="image image-2 paroller">
												<?php  if ( esc_url($settings['image2']['id']) ) : ?>   
												<img src="<?php echo wp_get_attachment_url($settings['image2']['id']);?>" alt="<?php echo esc_attr($settings['alt_text2']);?>"/>
												<?php else :?>
												<div class="noimage"></div>
												<?php endif;?>
											</figure>
										</div>
									</div>
									<div class="col-lg-6 col-md-12 col-sm-12 content-column">
										<div class="content-box ml_30">
											<h3><?php echo $settings['tab_one_title'];?></h3>
											<div class="text">
												<p><?php echo $settings['tab_one_text'];?></p>
												<p><?php echo $settings['tab_one_text2'];?></p>
											</div>
											<h4><?php echo $settings['tab_one_title2'];?></h4>
											<ul class="list clearfix">
												<?php foreach($settings['repeat'] as $item):?>
												<li><?php echo wp_kses($item['block_title'], $allowed_tags);?></li>
												<?php endforeach; ?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab" id="tab-2">
							<div class="content-inner">
								<div class="row clearfix">
									<div class="col-lg-6 col-md-12 col-sm-12 content-column">
										<div class="content-box mr_30">
											<h3><?php echo $settings['tab_two_title'];?></h3>
											<div class="text">
												<p><?php echo $settings['tab_two_text'];?></p>
												<p><?php echo $settings['tab_two_text2'];?></p>
											</div>
											<ul class="list style-two clearfix">
												<?php foreach($settings['repeat2'] as $item):?>
												<li><?php echo wp_kses($item['block_title'], $allowed_tags);?></li>
												<?php endforeach; ?>
											</ul>
										</div>
									</div>
									<div class="col-lg-6 col-md-12 col-sm-12 image-column">
										<div class="image-box">
											<?php  if ( esc_url($settings['bgimg4']['id']) ) : ?>
											<div class="image-shape-2 rotate-me" style="background-image: url(<?php echo wp_get_attachment_url($settings['bgimg4']['id']);?>);">
											<?php else :?>	
											<div class="noimage">
											<?php endif;?>
											</div>
											<figure class="image image-1">
												<?php  if ( esc_url($settings['image3']['id']) ) : ?>   
												<img src="<?php echo wp_get_attachment_url($settings['image3']['id']);?>" alt="<?php echo esc_attr($settings['alt_text3']);?>"/>
												<?php else :?>
												<div class="noimage"></div>
												<?php endif;?>
											</figure>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab" id="tab-3">
							<div class="content-inner">
								<div class="row clearfix">
									<div class="col-lg-6 col-md-12 col-sm-12 content-column">
										<div class="content-box mr_30">
											<h3><?php echo $settings['tab_three_title'];?></h3>
											<div class="text">
												<p><?php echo $settings['tab_three_text'];?></p>
											</div>
											<h4><?php echo $settings['tab_three_title2'];?></h4>
											<div class="text mb_0">
												<p><?php echo $settings['tab_three_text2'];?></p>
												<p><?php echo $settings['tab_three_text3'];?></p>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-12 col-sm-12 image-column">
										<div class="image-box pr_0">
											<?php  if ( esc_url($settings['bgimg5']['id']) ) : ?>
											<div class="image-shape-2 rotate-me" style="background-image: url(<?php echo wp_get_attachment_url($settings['bgimg5']['id']);?>);">
											<?php else :?>	
											<div class="noimage">
											<?php endif;?>
											</div>
											<div class="row clearfix">
												<div class="col-lg-6 col-md-12 col-sm-12 single-image">
													<figure class="image image-1 image-3">
														<?php  if ( esc_url($settings['image4']['id']) ) : ?>   
														<img src="<?php echo wp_get_attachment_url($settings['image4']['id']);?>" alt="<?php echo esc_attr($settings['alt_text4']);?>"/>
														<?php else :?>
														<div class="noimage"></div>
														<?php endif;?>
													</figure>
												</div>
												<div class="col-lg-6 col-md-12 col-sm-12 single-image">
													<div class="two-image">
														<figure class="image image-4">
														<?php  if ( esc_url($settings['image5']['id']) ) : ?>   
														<img src="<?php echo wp_get_attachment_url($settings['image5']['id']);?>" alt="<?php echo esc_attr($settings['alt_text5']);?>"/>
														<?php else :?>
														<div class="noimage"></div>
														<?php endif;?>
														</figure>
														<figure class="image image-5">
														<?php  if ( esc_url($settings['image6']['id']) ) : ?>   
														<img src="<?php echo wp_get_attachment_url($settings['image6']['id']);?>" alt="<?php echo esc_attr($settings['alt_text6']);?>"/>
														<?php else :?>
														<div class="noimage"></div>
														<?php endif;?>
														</figure>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab" id="tab-4">
							<div class="content-inner">
								<div class="row clearfix">
									<div class="col-lg-6 col-md-12 col-sm-12 image-column">
										<div class="image-box">
											<?php  if ( esc_url($settings['bgimg6']['id']) ) : ?>
											<div class="image-shape-1 rotate-me" style="background-image: url(<?php echo wp_get_attachment_url($settings['bgimg6']['id']);?>);">
											<?php else :?>	
											<div class="noimage">
											<?php endif;?>
											</div>
											<figure class="image image-1">
											<?php  if ( esc_url($settings['image7']['id']) ) : ?>   
											<img src="<?php echo wp_get_attachment_url($settings['image7']['id']);?>" alt="<?php echo esc_attr($settings['alt_text7']);?>"/>
											<?php else :?>
											<div class="noimage"></div>
											<?php endif;?>
											</figure>
											<figure class="image image-2">
											<?php  if ( esc_url($settings['image8']['id']) ) : ?>   
											<img src="<?php echo wp_get_attachment_url($settings['image8']['id']);?>" alt="<?php echo esc_attr($settings['alt_text8']);?>"/>
											<?php else :?>
											<div class="noimage"></div>
											<?php endif;?>
											</figure>
										</div>
									</div>
									<div class="col-lg-6 col-md-12 col-sm-12 content-column">
										<div class="content-box">
											<h3><?php echo $settings['tab_four_title'];?></h3>
											<div class="text">
												<p><?php echo $settings['tab_four_text'];?></p>
											</div>
											<h4><?php echo $settings['tab_four_title2'];?></h4>
											<ul class="list clearfix">
												<?php foreach($settings['repeat3'] as $item):?>
												<li><?php echo wp_kses($item['block_title'], $allowed_tags);?></li>
												<?php endforeach; ?>
											</ul>
											<?php if($settings['bttn']): ?>
											<div class="service-style1_btns-box">
												<a class="btn-one" href="<?php echo esc_url($settings['btnlink']['url']);?>">
													<span class="txt"><?php echo $settings['bttn'];?></span>
												</a>
											</div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
             
		<?php 
	}

}