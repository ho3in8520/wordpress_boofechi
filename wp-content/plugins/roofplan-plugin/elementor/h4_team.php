<?php

namespace ROOFPLANPLUGIN\Element;

use Elementor\Controls_Manager;
use Elementor\Controls_Stack;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Repeater;
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Plugin;

/**
 * Elementor button widget.
 * Elementor widget that displays a button with the ability to control every
 * aspect of the button design.
 *
 * @since 1.0.0
 */
class H4_Team extends Widget_Base {

	/**
	 * Get widget name.
	 * Retrieve button widget name.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'roofplan_h4_team';
	}

	/**
	 * Get widget title.
	 * Retrieve button widget title.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'H4 Team', 'roofplan' );
	}

	/**
	 * Get widget icon.
	 * Retrieve button widget icon.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-briefcase';
	}

	/**
	 * Get widget categories.
	 * Retrieve the list of categories the button widget belongs to.
	 * Used to determine where to display the widget in the editor.
	 *
	 * @since  2.0.0
	 * @access public
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'roofplan' ];
	}
	
	/**
	 * Register button widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since  1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'h4_team',
			[
				'label' => esc_html__( 'H4 Team', 'roofplan' ),
			]
		);
		$this->add_control(
			'sec_class',
			[
				'label'       => __( 'Section Class', 'rashid' ),
				'type'        => Controls_Manager::TEXTAREA,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Enter Section Class', 'rashid' ),
			]
		);
		$this->add_control(
			'bgimg',
			[
				'label' => esc_html__('Background image', 'rashid'),
				'type' => Controls_Manager::MEDIA,
				'default' => ['url' => Utils::get_placeholder_image_src(),],
			]
		);
		$this->add_control(
			'bgimg2',
			[
				'label' => esc_html__('Background image', 'rashid'),
				'type' => Controls_Manager::MEDIA,
				'default' => ['url' => Utils::get_placeholder_image_src(),],
			]
		);
		$this->add_control(
			'subtitle',
			[
				'label'       => __( 'Sub Title', 'rashid' ),
				'type'        => Controls_Manager::TEXTAREA,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Enter your Sub title', 'rashid' ),
			]
		);
		$this->add_control(
			'title',
			[
				'label'       => __( 'Title', 'rashid' ),
				'type'        => Controls_Manager::TEXTAREA,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Enter your title', 'rashid' ),
			]
		);
		$this->add_control(
			'column',
			[
				'label'   => esc_html__( 'Column', 'rashid' ),
				'type'    => Controls_Manager::SELECT,
				'default' => '3',
				'options' => array(
					'12'   => esc_html__( 'One Column', 'rashid' ),
					'6'   => esc_html__( 'Two Column', 'rashid' ),
					'4'   => esc_html__( 'Three Column', 'rashid' ),
					'3'   => esc_html__( 'Four Column', 'rashid' ),
					'2'   => esc_html__( 'Six Column', 'rashid' ),
				),
			]
		);

		$this->add_control(
			'text_limit',
			[
				'label'   => esc_html__( 'Text Limit', 'roofplan' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3,
				'min'     => 1,
				'max'     => 100,
				'step'    => 1,
			]
		);
		$this->add_control(
			'query_number',
			[
				'label'   => esc_html__( 'Number of post', 'roofplan' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3,
				'min'     => 1,
				'max'     => 100,
				'step'    => 1,
			]
		);
		$this->add_control(
			'query_orderby',
			[
				'label'   => esc_html__( 'Order By', 'roofplan' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'date',
				'options' => array(
					'date'       => esc_html__( 'Date', 'roofplan' ),
					'title'      => esc_html__( 'Title', 'roofplan' ),
					'menu_order' => esc_html__( 'Menu Order', 'roofplan' ),
					'rand'       => esc_html__( 'Random', 'roofplan' ),
				),
			]
		);
		$this->add_control(
			'query_order',
			[
				'label'   => esc_html__( 'Order', 'roofplan' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => array(
					'DESc' => esc_html__( 'DESC', 'roofplan' ),
					'ASC'  => esc_html__( 'ASC', 'roofplan' ),
				),
			]
		);
		$this->add_control(
			'query_exclude',
			[
				'label'       => esc_html__( 'Exclude', 'roofplan' ),
				'type'        => Controls_Manager::TEXT,
				'description' => esc_html__( 'Exclude posts, pages, etc. by ID with comma separated.', 'roofplan' ),
			]
		);
		$this->add_control(
            'query_category', 
				[
				  'type' => Controls_Manager::SELECT,
				  'label' => esc_html__('Category', 'roofplan'),
				  'options' => get_team_categories()
				]
		);
		$this->end_controls_section();
	}

	/**
	 * Render button widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since  1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
        
        $paged = roofplan_set($_POST, 'paged') ? esc_attr($_POST['paged']) : 1;

		$this->add_render_attribute( 'wrapper', 'class', 'templatepath-roofplan' );
		$args = array(
			'post_type'      => 'roofplan_team',
			'posts_per_page' => roofplan_set( $settings, 'query_number' ),
			'orderby'        => roofplan_set( $settings, 'query_orderby' ),
			'order'          => roofplan_set( $settings, 'query_order' ),
			'paged'         => $paged
		);
		if ( roofplan_set( $settings, 'query_exclude' ) ) {
			$settings['query_exclude'] = explode( ',', $settings['query_exclude'] );
			$args['post__not_in']      = roofplan_set( $settings, 'query_exclude' );
		}
		if( roofplan_set( $settings, 'query_category' ) ) $args['team_cat'] = roofplan_set( $settings, 'query_category' );
		$query = new \WP_Query( $args );

		if ( $query->have_posts() ) 
		{
		?>
  
		<section class="team-home-four <?php echo esc_attr($settings['sec_class']);?>">
			<div class="pattern-layer">
				<?php  if ( esc_url($settings['bgimg']['id']) ) : ?>
				<div class="pattern-1" style="background-image: url(<?php echo wp_get_attachment_url($settings['bgimg']['id']);?>);">
				<?php else :?>	
				<div class="noimage">
				<?php endif;?>
				</div>
				<?php  if ( esc_url($settings['bgimg2']['id']) ) : ?>
				<div class="pattern-2" style="background-image: url(<?php echo wp_get_attachment_url($settings['bgimg2']['id']);?>);">
				<?php else :?>	
				<div class="noimage">
				<?php endif;?>
				</div>
			</div>
			<div class="container">
				
				<div class="sec-title text-center">
					<div class="sub-title">
						<p><?php echo $settings['subtitle'];?></p>
					</div>
					<h2><?php echo $settings['title'];?></h2>
				</div>
				
				<div class="row clearfix">
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<div class="col-lg-<?php echo esc_attr($settings['column'], true );?> col-md-6 col-sm-12 single-column">
						<div class="team-block wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
							<div class="inner-box">
								<figure class="image-box">
									<?php the_post_thumbnail(); ?>
								</figure>
								<div class="lower-content">
									<div class="shape" style="background-image: url(<?php echo esc_url(get_template_directory_uri().'/assets/images/shape/shape-14.png');?>);"></div>
									<h4><?php the_title(); ?></h4>
									<span class="designation"><?php echo (get_post_meta( get_the_id(), 'designation', true ));?></span>
									<div class="share-box">
										<a href="#" class="share-icon"><i class="fa fa-share-alt"></i></a>
										<?php	
										$icons = get_post_meta( get_the_id(), 'social_profile', true );
											if ( ! empty( $icons ) ) :?>
										<ul class="share-links">
											<?php
											foreach ( $icons as $h_icon ) :
											$header_social_icons = json_decode( urldecode( roofplan_set( $h_icon, 'data' ) ) );
											if ( roofplan_set( $header_social_icons, 'enable' ) == '' ) {
											continue;
											}
											$icon_class = explode( '-', roofplan_set( $header_social_icons, 'icon' ) );?>
											<li><a href="<?php echo roofplan_set( $header_social_icons, 'url' ); ?>"><i class="fa <?php echo esc_attr( roofplan_set( $header_social_icons, 'icon' ) ); ?>"></i></a></li>
											<?php endforeach; ?>
										</ul>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php endwhile; ?>
				</div>
			</div>
		</section>
  
        <?php }
		wp_reset_postdata();
	}

}