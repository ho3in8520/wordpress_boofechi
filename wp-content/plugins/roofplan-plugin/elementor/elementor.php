<?php

namespace ROOFPLANPLUGIN\Element;


class Elementor {
	static $widgets = array(
		'h1_slider',
		'title',
		'h1_features',
		'h1_about',
		'h1_service_image',
		'h1_gallery',
		'h1_testimonials',
		'h1_working',
		'h1_funfact',
		'h1_team',
		'h1_faq_left',
		'h1_faq_right',
		'h1_contact',
		'h1_blog',
		'footer_widget1',
		'footer_widget2',
		'footer_widget3',
		'footer_widget4',
		'footer_bottom',
		'h2_slider',
		'h2_features',
		'h2_about_left',
		'h2_about_right',
		'h2_service_image',
		'h2_contact',
		'h2_gallery',
		'h2_testimonials',
		'h2_funfact',
		'h3_slider',
		'h3_contact',
		'h3_about_left',
		'h3_about_right',
		'h3_service_icon',
		'h3_choose',
		'h3_testimonials',
		'h3_video',
		'about_right',
		'wi_menu',
		'service_details',
		'service_details2',
		'service_icon',
		'team',
		'team_details_left',
		'team_details_right',
		'team_details1',
        'team_details2',
		'team_details3',
		'pricing',
		'gallery',
		'gallery2',
		'project_details',
		'project_details2',
		'project_details3',
		'project_details4',
		'faq_left',
		'faq_right',
		'blog_grid',
		'contact_info',
		'contact',
        'h4_slider',
		'h4_feature',
		'h4_about_left',
		'h4_about_right',
		'h4_tab',
		'h4_funfact',
		'h4_chooseus',
		'h4_testimonials',
		'h4_gallary',
		'h4_team',
		'h4_faq',
		'h4_workig',
		'h4_contact',
		'h4_chooseus_left',
		'h4_chooseus_right',
		'h4_faq_left',
		'h4_faq_right',
      	'h1_about_left',
		'h1_about_right',
        'h1_testimonials_left',
		'h1_testimonials_right',
        'h3_choose_left',
		'h3_choose_right',
        'h2_testimonials_left',
		'h2_testimonials_right',
	);

	static function init() {
		add_action( 'elementor/init', array( __CLASS__, 'loader' ) );
		add_action( 'elementor/elements/categories_registered', array( __CLASS__, 'register_cats' ) );
	}

	static function loader() {

		foreach ( self::$widgets as $widget ) {

			$file = ROOFPLANPLUGIN_PLUGIN_PATH . '/elementor/' . $widget . '.php';
			if ( file_exists( $file ) ) {
				require_once $file;
			}

			add_action( 'elementor/widgets/widgets_registered', array( __CLASS__, 'register' ) );
		}
	}

	static function register( $elemntor ) {
		foreach ( self::$widgets as $widget ) {
			$class = '\\ROOFPLANPLUGIN\\Element\\' . ucwords( $widget );

			if ( class_exists( $class ) ) {
				$elemntor->register_widget_type( new $class );
			}
		}
	}

	static function register_cats( $elements_manager ) {

		$elements_manager->add_category(
			'roofplan',
			[
				'title' => esc_html__( 'Roofplan', 'roofplan' ),
				'icon'  => 'fa fa-plug',
			]
		);
		$elements_manager->add_category(
			'csslatepath',
			[
				'title' => esc_html__( 'Template Path', 'roofplan' ),
				'icon'  => 'fa fa-plug',
			]
		);

	}
}

Elementor::init();