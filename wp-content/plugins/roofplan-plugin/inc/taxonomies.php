<?php

namespace ROOFPLANPLUGIN\Inc;


use ROOFPLANPLUGIN\Inc\Abstracts\Taxonomy;


class Taxonomies extends Taxonomy {


	public static function init() {

		$labels = array(
			'name'              => _x( 'Project Category', 'wproofplan' ),
			'singular_name'     => _x( 'Project Category', 'wproofplan' ),
			'search_items'      => __( 'Search Category', 'wproofplan' ),
			'all_items'         => __( 'All Categories', 'wproofplan' ),
			'parent_item'       => __( 'Parent Category', 'wproofplan' ),
			'parent_item_colon' => __( 'Parent Category:', 'wproofplan' ),
			'edit_item'         => __( 'Edit Category', 'wproofplan' ),
			'update_item'       => __( 'Update Category', 'wproofplan' ),
			'add_new_item'      => __( 'Add New Category', 'wproofplan' ),
			'new_item_name'     => __( 'New Category Name', 'wproofplan' ),
			'menu_name'         => __( 'Project Category', 'wproofplan' ),
		);
		$args   = array(
			'hierarchical'       => true,
			'labels'             => $labels,
			'show_ui'            => true,
			'show_admin_column'  => true,
			'query_var'          => true,
			'public'             => true,
			'publicly_queryable' => true,
			'rewrite'            => array( 'slug' => 'project_cat' ),
		);

		register_taxonomy( 'project_cat', 'roofplan_project', $args );
		
		//Services Taxonomy Start
		$labels = array(
			'name'              => _x( 'Service Category', 'wproofplan' ),
			'singular_name'     => _x( 'Service Category', 'wproofplan' ),
			'search_items'      => __( 'Search Category', 'wproofplan' ),
			'all_items'         => __( 'All Categories', 'wproofplan' ),
			'parent_item'       => __( 'Parent Category', 'wproofplan' ),
			'parent_item_colon' => __( 'Parent Category:', 'wproofplan' ),
			'edit_item'         => __( 'Edit Category', 'wproofplan' ),
			'update_item'       => __( 'Update Category', 'wproofplan' ),
			'add_new_item'      => __( 'Add New Category', 'wproofplan' ),
			'new_item_name'     => __( 'New Category Name', 'wproofplan' ),
			'menu_name'         => __( 'Service Category', 'wproofplan' ),
		);
		$args   = array(
			'hierarchical'       => true,
			'labels'             => $labels,
			'show_ui'            => true,
			'show_admin_column'  => true,
			'query_var'          => true,
			'public'             => true,
			'publicly_queryable' => true,
			'rewrite'            => array( 'slug' => 'service_cat' ),
		);


		register_taxonomy( 'service_cat', 'roofplan_service', $args );
		
		//Testimonials Taxonomy Start
		$labels = array(
			'name'              => _x( 'Testimonials Category', 'wproofplan' ),
			'singular_name'     => _x( 'Testimonials Category', 'wproofplan' ),
			'search_items'      => __( 'Search Category', 'wproofplan' ),
			'all_items'         => __( 'All Categories', 'wproofplan' ),
			'parent_item'       => __( 'Parent Category', 'wproofplan' ),
			'parent_item_colon' => __( 'Parent Category:', 'wproofplan' ),
			'edit_item'         => __( 'Edit Category', 'wproofplan' ),
			'update_item'       => __( 'Update Category', 'wproofplan' ),
			'add_new_item'      => __( 'Add New Category', 'wproofplan' ),
			'new_item_name'     => __( 'New Category Name', 'wproofplan' ),
			'menu_name'         => __( 'Testimonials Category', 'wproofplan' ),
		);
		$args   = array(
			'hierarchical'       => true,
			'labels'             => $labels,
			'show_ui'            => true,
			'show_admin_column'  => true,
			'query_var'          => true,
			'public'             => true,
			'publicly_queryable' => true,
			'rewrite'            => array( 'slug' => 'testimonials_cat' ),
		);


		register_taxonomy( 'testimonials_cat', 'roofplan_testimonials', $args );
		
		
		//Team Taxonomy Start
		$labels = array(
			'name'              => _x( 'Team Category', 'wproofplan' ),
			'singular_name'     => _x( 'Team Category', 'wproofplan' ),
			'search_items'      => __( 'Search Category', 'wproofplan' ),
			'all_items'         => __( 'All Categories', 'wproofplan' ),
			'parent_item'       => __( 'Parent Category', 'wproofplan' ),
			'parent_item_colon' => __( 'Parent Category:', 'wproofplan' ),
			'edit_item'         => __( 'Edit Category', 'wproofplan' ),
			'update_item'       => __( 'Update Category', 'wproofplan' ),
			'add_new_item'      => __( 'Add New Category', 'wproofplan' ),
			'new_item_name'     => __( 'New Category Name', 'wproofplan' ),
			'menu_name'         => __( 'Team Category', 'wproofplan' ),
		);
		$args   = array(
			'hierarchical'       => true,
			'labels'             => $labels,
			'show_ui'            => true,
			'show_admin_column'  => true,
			'query_var'          => true,
			'public'             => true,
			'publicly_queryable' => true,
			'rewrite'            => array( 'slug' => 'team_cat' ),
		);


		register_taxonomy( 'team_cat', 'roofplan_team', $args );
		
		//Faqs Taxonomy Start
		$labels = array(
			'name'              => _x( 'Faqs Category', 'wproofplan' ),
			'singular_name'     => _x( 'Faq Category', 'wproofplan' ),
			'search_items'      => __( 'Search Category', 'wproofplan' ),
			'all_items'         => __( 'All Categories', 'wproofplan' ),
			'parent_item'       => __( 'Parent Category', 'wproofplan' ),
			'parent_item_colon' => __( 'Parent Category:', 'wproofplan' ),
			'edit_item'         => __( 'Edit Category', 'wproofplan' ),
			'update_item'       => __( 'Update Category', 'wproofplan' ),
			'add_new_item'      => __( 'Add New Category', 'wproofplan' ),
			'new_item_name'     => __( 'New Category Name', 'wproofplan' ),
			'menu_name'         => __( 'Faq Category', 'wproofplan' ),
		);
		$args   = array(
			'hierarchical'       => true,
			'labels'             => $labels,
			'show_ui'            => true,
			'show_admin_column'  => true,
			'query_var'          => true,
			'public'             => true,
			'publicly_queryable' => true,
			'rewrite'            => array( 'slug' => 'faqs_cat' ),
		);


		register_taxonomy( 'faqs_cat', 'roofplan_faqs', $args );
	}
	
}
