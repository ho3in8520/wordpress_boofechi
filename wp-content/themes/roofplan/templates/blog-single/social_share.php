<?php 

$options = roofplan_WSH()->option();
$allowed_html = wp_kses_allowed_html(); ?>



<?php if($options->get('single_post_share' ) ): ?>
<div class="blog-details-social-links">
	<div class="inner-title">
		<h3><?php esc_html_e( 'We Are Social On:', 'roofplan' ); ?></h3>
	</div>  
	<div class="post-social-link">
		<?php if ( $options->get( 'single_social_share' ) && $options->get( 'single_post_share' ) ) : ?>
		<ul>
			<?php foreach ( $options->get( 'single_social_share' ) as $k => $v ) {
				if ( $v == '' ) {
					continue;
				} ?>
				<?php do_action('roofplan_social_share_output', $k ); ?>
			<?php } ?>
		</ul>
		<?php endif; ?>
	</div>
</div>
 <?php endif; ?>