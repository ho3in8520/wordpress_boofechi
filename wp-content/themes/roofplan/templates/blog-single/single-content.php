<?php
/**
* Single Post Content Template
*
* @package    WordPress
* @subpackage ROOFPLAN
* @author     Tona Theme
* @version    1.0
*/
?>
<?php global $wp_query;

$options = roofplan_WSH()->option();
$allowed_html = wp_kses_allowed_html();

$page_id = ( $wp_query->is_posts_page ) ? $wp_query->queried_object->ID : get_the_ID();

$gallery = get_post_meta( $page_id, 'roofplan_gallery_images', true );

$video = get_post_meta( $page_id, 'roofplan_video_url', true );


$audio_type = get_post_meta( $page_id, 'roofplan_audio_type', true );

?>

<div class="blog-details-content">

	<div class="single-blog-style1">
		<div class="img-holder">
			<div class="inner">
			<?php if($options->get('single_post_thumb' ) ): ?>		
				<?php the_post_thumbnail(); ?>
			<?php endif; ?>	
			<?php if($options->get('single_post_date' ) ): ?>	
				<div class="date-box">
					<div class="shape-bg" style="background-image: url(<?php echo esc_url(get_template_directory_uri().'/assets/images/shape/blog-shape-1.png');?>);"></div>
					<?php if($options->get('single_post_date' ) ): ?>
					<h5><a href="<?php echo get_day_link( get_the_time( 'Y' ), get_the_time( 'm' ), get_the_time( 'd' ) ); ?>"><?php echo get_the_date('d'); ?> <?php echo get_the_date('M'); ?></a></h5>
					<?php endif; ?>
				</div>
			<?php endif;?>	
			</div>
		</div>
	
		<div class="text-holder">
		<ul class="meta-info met_post">
				<?php if($options->get('single_post_author' ) ): ?>	
				<li><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta('ID') )); ?>"><?php the_author(); ?></a></li>
			<?php endif; ?>
				<?php if($options->get('single_post_date' ) ): ?>	
				<li><a href="<?php echo get_day_link( get_the_time( 'Y' ), get_the_time( 'm' ), get_the_time( 'd' ) ); ?>"><?php echo get_the_date(); ?></a></li>
			<?php endif; ?>
				<?php if($options->get('single_post_cat' ) ): ?>	
				<li><?php the_category(', '); ?></li>
			<?php endif; ?>
			</ul>
		</div> 
	</div>
		<?php if($options->get('single_post_title' ) ): ?>	
		<h3 class="blog-title">
				<?php the_title(); ?>
			</h3>
			<?php endif;?>	

	<div class="text">
		<?php the_content(); ?>
		<div class="clearfix"></div>
		<?php wp_link_pages(array('before'=>'<div class="paginate_links">'.esc_html__('Pages: ', 'roofplan'), 'after' => '</div>', 'link_before'=>'', 'link_after'=>'')); ?>
	</div>

	<?php roofplan_template_load( 'templates/blog-single/social_share.php', compact( 'options', 'data' ) ); ?>

	<?php comments_template(); ?> 


</div>