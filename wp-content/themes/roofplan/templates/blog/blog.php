<?php
$options = roofplan_WSH()->option();
$allowed_html = wp_kses_allowed_html();

/**
 * Blog Content Template
 *
 * @package    WordPress
 * @subpackage ROOFPLAN
 * @author     Tona Theme
 * @version    1.0
 */

if ( class_exists( 'Roofplan_Resizer' ) ) {
	$img_obj = new Roofplan_Resizer();
} else {
	$img_obj = array();
}
$allowed_tags = wp_kses_allowed_html('post');
global $post;
?>
<div <?php post_class(); ?>>


	<div class="single-blog-style1 wow fadeInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
		<div class="img-holder">
			<div class="inner">
				<a href="<?php echo esc_url(get_permalink(get_the_id()));?>">	<?php the_post_thumbnail(); ?></a>
					<?php if($options->get('blog_post_date' ) ): ?>
				<div class="date-box">
					<div class="shape-bg" style="background-image: url(<?php echo esc_url(get_template_directory_uri().'/assets/images/shape/blog-shape-1.png');?>);"></div>
					<h5><a href="<?php echo get_day_link( get_the_time( 'Y' ), get_the_time( 'm' ), get_the_time( 'd' ) ); ?>"><?php echo get_the_date('d'); ?> <?php echo get_the_date('M'); ?></a></h5>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="text-holder">
			<ul class="meta-info">
				<?php if($options->get('blog_post_author' ) ): ?>
				<li><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta('ID') )); ?>"><?php the_author(); ?></a></li>
				<?php endif;?>
				<?php if($options->get('blog_post_date' ) ): ?>
				<li><a href="<?php echo get_day_link( get_the_time( 'Y' ), get_the_time( 'm' ), get_the_time( 'd' ) ); ?>"><?php echo get_the_date(); ?></a></li>
				<?php endif;?>
				<?php if($options->get('blog_post_comment' ) ): ?>
				<li><?php comments_number(); ?></li>	
				<?php endif;?>
			</ul>
			<h3 class="blog-title">
				<a href="<?php echo esc_url( the_permalink( get_the_id() ) );?>"><?php the_title(); ?></a>
			</h3>
			<div class="text">
				<?php the_excerpt(); ?>
			</div>	
			<?php if($options->get('blog_post_readmore' ) ): ?>	
			<div class="readmore-button">
				<a href="<?php echo esc_url(get_permalink(get_the_id()));?>"><span class="flaticon-left-arrow"></span></a>
			</div>
			<!--mjjjjjjjjjjjjjjjj-->
			<?php endif; ?>
		</div> 
	</div>
    
</div>