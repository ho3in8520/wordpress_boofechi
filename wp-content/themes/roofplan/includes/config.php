<?php
/**
 * Theme config file.
 *
 * @package ROOFPLAN
 * @author  ThemeKalia
 * @version 1.0
 * changed
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
}

$config = array();

$config['default']['roofplan_main_header'][] 	= array( 'roofplan_preloader', 98 );
$config['default']['roofplan_main_header'][] 	= array( 'roofplan_main_header_area', 99 );

$config['default']['roofplan_main_footer'][] 	= array( 'roofplan_preloader', 98 );
$config['default']['roofplan_main_footer'][] 	= array( 'roofplan_main_footer_area', 99 );

$config['default']['roofplan_sidebar'][] 	    = array( 'roofplan_sidebar', 99 );

$config['default']['roofplan_banner'][] 	    = array( 'roofplan_banner', 99 );


return $config;
