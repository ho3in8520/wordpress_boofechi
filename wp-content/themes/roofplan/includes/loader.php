<?php

define( 'ROOFPLAN_ROOT', get_template_directory() . '/' );

require_once get_template_directory() . '/includes/functions/functions.php';
include_once get_template_directory() . '/includes/classes/base.php';
include_once get_template_directory() . '/includes/classes/dotnotation.php';
include_once get_template_directory() . '/includes/classes/header-enqueue.php';
include_once get_template_directory() . '/includes/classes/options.php';
include_once get_template_directory() . '/includes/classes/ajax.php';
include_once get_template_directory() . '/includes/classes/common.php';
include_once get_template_directory() . '/includes/classes/bootstrap_walker.php';
include_once get_template_directory() . '/includes/library/class-tgm-plugin-activation.php';
require_once get_template_directory() . '/includes/library/hook.php';

// Merlin demo import.
require_once get_template_directory() . '/demo-import/class-merlin.php';
require_once get_template_directory() . '/demo-import/merlin-config.php';
require_once get_template_directory() . '/demo-import/merlin-filters.php';

add_action( 'after_setup_theme', 'roofplan_wp_load', 5 );

function roofplan_wp_load() {

	defined( 'ROOFPLAN_URL' ) or define( 'ROOFPLAN_URL', get_template_directory_uri() . '/' );
	define(  'ROOFPLAN_KEY','!@#roofplan');
	define(  'ROOFPLAN_URI', get_template_directory_uri() . '/');

	if ( ! defined( 'ROOFPLAN_NONCE' ) ) {
		define( 'ROOFPLAN_NONCE', 'roofplan_wp_theme' );
	}

	( new \ROOFPLAN\Includes\Classes\Base )->loadDefaults();
	( new \ROOFPLAN\Includes\Classes\Ajax )->actions();

}
add_action( 'init', 'roofplan_bunch_theme_init');
function roofplan_bunch_theme_init()
{
	$bunch_exlude_hooks = include_once get_template_directory(). '/includes/resource/remove_action.php';
	

}
