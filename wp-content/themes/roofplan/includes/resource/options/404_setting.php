<?php

return array(
	'title'      => esc_html__( 'تنظیمات صفحه ۴۰۴', 'روف پلن' ),
	'id'         => '404_setting',
	'desc'       => '',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'      => '404_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سورس ۴۰۴', 'روف پلن' ),
			'options' => array(
				'd' => esc_html__( 'پیش فرض', 'روف پلن' ),
				'e' => esc_html__( 'المنتور', 'روف پلن' ),
			),
			'default' => 'd',
		),
		array(
			'id'       => '404_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'روف پلن' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
			],
			'required' => [ '404_source_type', '=', 'e' ],
		),
		array(
			'id'       => '404_default_st',
			'type'     => 'section',
			'title'    => esc_html__( '۴۰۴ پیشفرض', 'روف پلن' ),
			'indent'   => true,
			'required' => [ '404_source_type', '=', 'd' ],
		),
		array(
			'id'      => '404_page_banner',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش بنر', 'روف پلن' ),
			'desc'    => esc_html__( 'فعال کنید تا بنر روی بلاگ نمایش داده شود', 'روف پلن' ),
			'default' => true,
		),
		array(
			'id'       => '404_banner_title',
			'type'     => 'text',
			'title'    => esc_html__( 'عنوان بخش بنر', 'روف پلن' ),
			'desc'     => esc_html__( 'عنوان را برای نماش در بخش بنر وارد کنید', 'روف پلن' ),
			'required' => array( '404_page_banner', '=', true ),
		),
		array(
			'id'       => '404_page_breadcrumb',
			'type'     => 'raw',
			'content'  => "<div style='background-color:#c33328;color:white;padding:20px;'>" . esc_html__( 'از افزونه یوست سئو برای بریدکرامب استفاده کنید', 'روف پلن' ) . "</div>",
			'required' => array( '404_page_banner', '=', true ),
		),
		array(
			'id'       => '404_page_background',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'تصویر بنر', 'روف پلن' ),
			'desc'     => esc_html__( 'وارد کردن تصویر پس زمینه برای بنر', 'روف پلن' ),
			'required' => array( '404_page_banner', '=', true ),
		),

		array(
			'id'    => '404_title',
			'type'  => 'text',
			'title' => esc_html__( '۴۰۴', 'روف پلن' ),
			'desc'  => esc_html__( '۴۰۴ را وارد کنید', 'روف پلن' ),

		),
		array(
			'id'    => '404_page_title',
			'type'  => 'text',
			'title' => esc_html__( 'عنوان ۴۰۴', 'روف پلن' ),
			'desc'  => esc_html__( 'برای نمایش دادن . عنوان۴۰۴  را وارد کنید', 'روف پلن' ),

		),
		array(
			'id'    => '404_page_text',
			'type'  => 'textarea',
			'title' => esc_html__( 'توضیحات صفحه ۴۰۴', 'روف پلن' ),
			'desc'  => esc_html__( 'توضیحات صفحه ۴۰۴ را وارد کنید', 'روف پلن' ),

		),
		/*array(
			'id'    => '404_page_form',
			'type'  => 'switch',
			'title' => esc_html__( 'Show Search Form', 'روف پلن' ),
			'desc'  => esc_html__( 'Enable to show search form on 404 page', 'روف پلن' ),
			'default'  => true,
		),*/
		array(
			'id'    => 'back_home_btn',
			'type'  => 'switch',
			'title' => esc_html__( 'نمایش دکمه', 'روف پلن' ),
			'desc'  => esc_html__( 'نمایش دکمه بازگشت به خانه', 'روف پلن' ),
			'default'  => true,
		),
		array(
			'id'       => 'back_home_btn_label',
			'type'     => 'text',
			'title'    => esc_html__( 'لیبل دکمه', 'روف پلن' ),
			'desc'     => esc_html__( 'عنوان خود را برای دکمه بازگشت به خانه وارد کنید', 'روف پلن' ),
			'default'  => esc_html__( 'بازگشت به صفحه خانه', 'روف پلن' ),
			'required' => array( 'back_home_btn', '=', true ),
		),
		
		

		array(
			'id'     => '404_post_settings_end',
			'type'   => 'section',
			'indent' => false,
		),

	),
);





