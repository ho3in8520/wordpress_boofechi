<?php

return array(
	'title'      => esc_html__( 'تنظیمات صفحه نویسنده', 'روف پلن' ),
	'id'         => 'author_setting',
	'desc'       => '',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'      => 'author_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سورس ', 'روف پلن' ),
			'options' => array(
				'd' => esc_html__( 'پیش فرض', 'روف پلن' ),
				'e' => esc_html__( 'المنتور', 'روف پلن' ),
			),
			'default' => 'd',
		),
		array(
			'id'       => 'author_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'روف پلن' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
				'posts_per_page'=> -1,
			],
			'required' => [ 'author_source_type', '=', 'e' ],
		),

		array(
			'id'       => 'author_default_st',
			'type'     => 'section',
			'title'    => esc_html__( 'نویسنده پیش فرض', 'روف پلن' ),
			'indent'   => true,
			'required' => [ 'author_source_type', '=', 'd' ],
		),
		array(
			'id'      => 'author_page_banner',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش بنر', 'روف پلن' ),
			'desc'    => esc_html__( 'برای نمایش فعال کنید', 'روف پلن' ),
			'default' => true,
		),
		array(
			'id'       => 'author_banner_title',
			'type'     => 'text',
			'title'    => esc_html__( 'عنوان بخش بنر', 'روف پلن' ),
			'desc'     => esc_html__( 'عنوان را برای نمایش در بنر وارد کنید', 'روف پلن' ),
			'required' => array( 'author_page_banner', '=', true ),
		),
		array(
			'id'       => 'author_page_background',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'تصویر پس زمینه', 'روف پلن' ),
			'desc'     => esc_html__( 'تصویر پس زمینه برای بنز وارد کنید', 'روف پلن' ),
			'default'  => '',
			'required' => array( 'author_page_banner', '=', true ),
		),
		array(
			'id'       => 'author_sidebar_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'لایه بندی', 'روف پلن' ),
			'subtitle' => esc_html__( 'محتوای اصلی و چینش سایدبار را انتخاب کنید', 'روف پلن' ),
			'options'  => array(

				'left'  => array(
					'alt' => esc_html__( 'دو ستون چپ', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cl.png',
				),
				'full'  => array(
					'alt' => esc_html__( 'تک ستون', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/1col.png',
				),
				'right' => array(
					'alt' => esc_html__( 'دو ستونه راست', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cr.png',
				),
			),

			'default' => 'right',
		),

		array(
			'id'       => 'author_page_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'سایدبار', 'روف پلن' ),
			'desc'     => esc_html__( 'سایدبار را برای نمایش در بلاگ لیستی فعال کنید', 'روف پلن' ),
			'required' => array(
				array( 'author_sidebar_layout', '=', array( 'left', 'right' ) ),
			),
			'options'  => roofplan_get_sidebars(),
		),
		array(
			'id'       => 'author_default_ed',
			'type'     => 'section',
			'indent'   => false,
			'required' => [ 'author_source_type', '=', 'd' ],
		),
	),
);





