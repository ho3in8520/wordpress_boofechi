<?php

return array(
	'title'      => esc_html__( 'تنظیمات صفحه فروشگاه', 'روف پلن' ),
	'id'         => 'shop_setting',
	'desc'       => '',
	'icon'       => ' fa fa-shopping-cart',
	'subsection' => false,
	'fields'     => array(
		array(
			'id'      => 'shop_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سورس فروشگاه', 'روف پلن' ),
			'options' => array(
				'd' => esc_html__( 'پیش فرض', 'روف پلن' ),
				'e' => esc_html__( 'المنتور', 'روف پلن' ),
			),
			'default' => 'd',
		),
		array(
			'id'       => 'shop_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'روف پلن' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
				'posts_per_page'=> -1,
			],
			'required' => [ 'shop_source_type', '=', 'e' ],
		),

		array(
			'id'       => 'shop_default_st',
			'type'     => 'section',
			'title'    => esc_html__( 'فروشگاه پیش فرض', 'روف پلن' ),
			'indent'   => true,
			'required' => [ 'shop_source_type', '=', 'd' ],
		),
		array(
			'id'      => 'shop_page_banner',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش بنر', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
		),
		array(
			'id'       => 'shop_banner_title',
			'type'     => 'text',
			'title'    => esc_html__( 'عنوان بخش بنر', 'روف پلن' ),
			'desc'     => esc_html__( '', 'روف پلن' ),
			'required' => array( 'shop_page_banner', '=', true ),
		),
	
		array(
			'id'       => 'shop_page_background',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'تصویر پس زمینه', 'روف پلن' ),
			'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => array(
				'url' => ROOFPLAN_URI . 'assets/images/pagetitle.jpg',
			),
			'required' => array( 'shop_page_banner', '=', true ),
		),

		array(
			'id'       => 'shop_sidebar_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'لایه بندی', 'روف پلن' ),
			'subtitle' => esc_html__( '', 'روف پلن' ),
			'options'  => array(

				'left'  => array(
					'alt' => esc_html__( 'دو ستون چپ', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cl.png',
				),
				'full'  => array(
					'alt' => esc_html__( 'تک ستونه', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/1col.png',
				),
				'right' => array(
					'alt' => esc_html__( 'دو ستونه راست', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cr.png',
				),
			),

			'default' => 'right',
		),

		array(
			'id'       => 'shop_page_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'سایدبار', 'روف پلن' ),
			'desc'     => esc_html__( '', 'روف پلن' ),
			'required' => array(
				array( 'shop_sidebar_layout', '=', array( 'left', 'right' ) ),
			),
'options'  => ROOFPLAN_get_sidebars(),
		),
	

		  array (
        'id'       => 'shop_column',
        'type'     => 'select',
        'title'    => __('ستون فروشگاه', 'روف پلن'), 
        'desc'     => __('', 'روف پلن'),
         // Must provide key => value pairs for select options
        'options'  => array(
            '6' => 'دو ستونه',
            '4' => 'سه ستونه',
            '3' => 'چهار ستونه',
			'2' => 'شش ستونه',
            ),
        'default'  => '2',
    ),
	  array (
        'id'       => 'shop_product',
        'type'     => 'text',
        'title'    => __('تعداد محصول', 'روف پلن'), 
        'desc'     => __('', 'روف پلن'),
        'default'  => '8',
    ),

		array(
			'id'       => 'shop_default_ed',
			'type'     => 'section',
			'indent'   => false,
			'required' => [ 'shop_source_type', '=', 'd' ],
		),
	),
);





