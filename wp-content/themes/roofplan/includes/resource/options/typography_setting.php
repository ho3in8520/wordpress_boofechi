<?php

return array(

    'title'         => esc_html__( 'تنظیمات تایپوگرافی', 'roofplan' ),
    'id'            => 'typography_setting',
    'desc'          => '',
    'icon'          => 'el el-edit'
);