<?php
return array(
	'title'      => esc_html__( 'تنظیمات هدر یک', 'روف پلن' ),
	'id'         => 'header_setting',
	'desc'       => '',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'      => 'logo_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'استایل لوگو', 'روف پلن' ),
			'desc'    => esc_html__( 'سبک لوگوی هر مدلی را برای نمایش در سرصفحه انتخاب کنید', 'روف پلن' ),
			'options' => array(
				'image' => esc_html__( 'تصویر لوگو', 'روف پلن' ),
				'text'  => esc_html__( 'متن لوگو', 'روف پلن' ),
			),
			'default' => 'image',
		),
		array(
			'id'       => 'image_logo',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'لوگو', 'روف پلن' ),
			'subtitle' => esc_html__( 'تصویر لوگوی سایت را با اندازه قابل تنظیم برای قسمت لوگو درج کنید', 'روف پلن' ),
			'default'  => array( 'url' => get_template_directory_uri() . '/assets/images/logo-2.png' ),
			'required' => array( array( 'logo_type', 'equals', 'image' ) ),
		),
		array(
			'id'       => 'logo_dimension',
			'type'     => 'dimensions',
			'title'    => esc_html__( 'ابعاد لوگو', 'روف پلن' ),
			'subtitle' => esc_html__( 'ابعاد لوگو را انتخاب کنید', 'روف پلن' ),
			'units'    => array( 'em', 'px', '%' ),
			'default'  => array( 'Width' => '', 'Height' => '' ),
			'required' => array(
				array( 'logo_type', 'equals', 'image' ),
			),
		),
		array(
			'id'       => 'logo_text',
			'type'     => 'text',
			'title'    => esc_html__( 'متن لوگو', 'روف پلن' ),
			'subtitle' => esc_html__( 'متن لوگو را وارد کنید', 'روف پلن' ),
			'required' => array(
				array( 'logo_type', 'equals', 'text' ),
			),
		),
		array(
			'id'          => 'logo_typography',
			'type'        => 'typography',
			'title'       => esc_html__( 'Typography', 'روف پلن' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
			'line-height' => false,
			'output'      => array( 'h2.site-description' ),
			'units'       => 'px',
			'subtitle'    => esc_html__( 'انتخاب استایل برای متن لوگو', 'روف پلن' ),
			'default'     => array(
				'color'       => '#333',
				'font-style'  => '700',
				'font-family' => 'Abel',
				'google'      => true,
				'font-size'   => '33px',
			),
			'required'    => array(
				array( 'logo_type', 'equals', 'text' ),
			),
		),

		array(
			'id'    => 'header_social_share',
			'type'  => 'social_media',
			'title' => esc_html__( 'شبکه های اجتماعی', 'روف پلن' ),
			'desc'  => esc_html__( 'برای فعال کردن نمادهای شبکه اجتماعی در هدر ، روی یک نماد کلیک کنید.', 'روف پلن' ),
		),
	),
);
