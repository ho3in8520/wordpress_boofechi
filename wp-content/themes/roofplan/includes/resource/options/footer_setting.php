<?php

return array(
	'title'      => esc_html__( 'تنظیمات فوتر', 'روف پلن' ),
	'id'         => 'footer_setting',
	'desc'       => '',
	'subsection' => false,
	'fields'     => array(
		array(
			'id'      => 'footer_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سورس فوتر', 'روف پلن' ),
			'options' => array(
				'd' => esc_html__( 'پیش فرض', 'روف پلن' ),
				'e' => esc_html__( 'المنتور', 'روف پلن' ),
			),
			'default' => 'd',
		),
		array(
			'id'       => 'footer_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'روف پلن' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
				'posts_per_page'	=> -1
			],
			'required' => [ 'footer_source_type', '=', 'e' ],
		),
		array(
			'id'       => 'footer_style_section_start',
			'type'     => 'section',
			'indent'      => true,
			'title'    => esc_html__( 'تنظیمات فوتر', 'روف پلن' ),
			'required' => array( 'footer_source_type', '=', 'd' ),
		),
		array(
		    'id'       => 'footer_style_settings',
		    'type'     => 'image_select',
		    'title'    => esc_html__( 'انتخاب استایل فوتر', 'روف پلن' ),
		    'subtitle' => esc_html__( 'انتخاب استایل فوتر', 'روف پلن' ),
		    'options'  => array(

			    'footer_v1'  => array(
				    'alt' => esc_html__( 'استایل یک فوتر', 'روف پلن' ),
				    'img' => get_template_directory_uri() . '/assets/images/redux/footer/footer1.png',
			    ),
			   /* 'footer_v2'  => array(
				    'alt' => esc_html__( 'Footer RTL Style', 'روف پلن' ),
				    'img' => get_template_directory_uri() . '/assets/images/redux/footer/footer2.png',
			    ), */
			),
			'required' => array( 'footer_source_type', '=', 'd' ),
			'default' => 'footer_v1',
	    ),
		/***********************************************************************
								Footer Version 1 Start
		************************************************************************/
		array(
			'id'       => 'footer_v1_settings_section_start',
			'type'     => 'section',
			'indent'      => true,
			'title'    => esc_html__( 'تنظیمات استایل فوتر', 'روف پلن' ),
			'required' => array( 'footer_style_settings', '=', 'footer_v1' ),
		),
		

		array(
			'id'      => 'copyright_text',
			'type'    => 'textarea',
			'title'   => __( 'متن کپی رایت', 'روف پلن' ),
			'desc'    => esc_html__( 'متن کپی رایت را وارد کنید', 'روف پلن' ),
			'required' => array( 'footer_style_settings', '=', 'footer_v1' ),
		),
		/***********************************************************************
								Footer Version 2 Start
		************************************************************************/
		array(
			'id'       => 'footer_v2_settings_section_start',
			'type'     => 'section',
			'indent'      => true,
			'title'    => esc_html__( 'تنظیمات استایل فوتر راست چین', 'روف پلن' ),
			'required' => array( 'footer_style_settings', '=', 'footer_v2' ),
		),
		array(
		    'id'       => 'show_news_letter_area_v2',
		    'type'     => 'switch',
		    'title'    => esc_html__( 'فعال کردن ناحیه حروف جدید', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => '',
		    'required' => array( 'footer_style_settings', '=', 'footer_v2' ),
	    ),
		array(
			'id'    => 'footer_v2_social_share',
			'type'  => 'social_media',
			'title' => esc_html__( 'شبکه های اجتماعی', 'روف پلن' ),
			'required' => array( 'footer_style_settings', '=', 'footer_v2' ),
		),
		array(
			'id'      => 'newletter_form_title_v2',
			'type'    => 'text',
			'title'   => __( 'عنوان فرم خبرنامه	', 'روف پلن' ),
			'desc'    => esc_html__( 'عنوان فرم خبرنامه را وارد کنید', 'روف پلن' ),
			'required' => array( 'footer_style_settings', '=', 'footer_v2' ),
		),
		array(
			'id'      => 'newletter_form_id_v2',
			'type'    => 'text',
			'title'   => __( 'فرم خبرنامه از ID', 'روف پلن' ),
			'desc'    => esc_html__( 'Enter the Form ID', 'روف پلن' ),
			'required' => array( 'footer_style_settings', '=', 'footer_v2' ),
		),
		array(
			'id'       => 'footer_shape_three',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'تصویر شکل پس زمینه', 'روف پلن' ),
			'required' => array( 'footer_style_settings', '=', 'footer_v2' ),
		),
		array(
			'id'       => 'footer_shape_four',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'تصویر شکل پس زمینه دو', 'روف پلن' ),
			'required' => array( 'footer_style_settings', '=', 'footer_v2' ),
		),
		array(
			'id'      => 'copyright_text2',
			'type'    => 'textarea',
			'title'   => __( 'متن کپی رایت', 'روف پلن' ),
			'desc'    => esc_html__( 'متن کپی رایت را وارد کنید', 'روف پلن' ),
			'required' => array( 'footer_style_settings', '=', 'footer_v2' ),
		),
		array(
			'id'       => 'footer_default_ed',
			'type'     => 'section',
			'indent'   => false,
			'required' => [ 'footer_source_type', '=', 'd' ],
		),
	),
);