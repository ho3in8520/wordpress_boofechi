<?php
return array(
	'title'      => esc_html__( 'تنظیمات لوگو', 'روف پلن' ),
	'id'         => 'logo_setting',
	'desc'       => '',
	'subsection' => false,
	'fields'     => array(
		array(
			'id'       => 'image_favicon',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'Favicon', 'روف پلن' ),
			'subtitle' => esc_html__( '', 'روف پلن' ),
			'default'  => array( 'url' => get_template_directory_uri() . '/assets/images/favicon.ico' ),
		),
	
		array(
			'id'       => 'image_normal_logo',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'لوگو یک', 'روف پلن' ),
			'subtitle' => esc_html__( 'تصویر دارک لوگو', 'روف پلن' ),
	
		),
		array(
			'id'       => 'normal_logo_dimension',
			'type'     => 'dimensions',
			'title'    => esc_html__( ' ابعاد لوگو', 'روف پلن' ),
			'subtitle' => esc_html__( 'ابعاد لوگو دارک', 'روف پلن' ),
			'units'    => array( 'em', 'px', '%' ),
			'default'  => array( 'Width' => '', 'Height' => '' ),
			'required' => array( 'normal_logo_show', '=', true ),
		),

		array(
			'id'       => 'image_normal_logo2',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'لوگو دو', 'روف پلن' ),
			'subtitle' => esc_html__( 'تصویر لوگو لایت', 'روف پلن' ),
		),
		array(
			'id'       => 'normal_logo_dimension2',
			'type'     => 'dimensions',
			'title'    => esc_html__( ' ابعاد لوگو', 'روف پلن' ),
			'subtitle' => esc_html__( 'انتخاب ابعاد لوگو', 'روف پلن' ),
			'units'    => array( 'em', 'px', '%' ),
			'default'  => array( 'Width' => '', 'Height' => '' ),
			'required' => array( 'normal_logo_show2', '=', true ),
		),
		array(
			'id'       => 'image_normal_logo3',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'لوگو سه', 'روف پلن' ),
			'subtitle' => esc_html__( 'تصویر لوگو موبایل', 'روف پلن' ),
		),
		array(
			'id'       => 'normal_logo_dimension3',
			'type'     => 'dimensions',
			'title'    => esc_html__( ' ابعاد لوگو موبایل', 'روف پلن' ),
			'subtitle' => esc_html__( '', 'روف پلن' ),
			'units'    => array( 'em', 'px', '%' ),
			'default'  => array( 'Width' => '', 'Height' => '' ),
			'required' => array( 'normal_logo_show3', '=', true ),
		),
		array(
			'id'       => 'image_normal_logo4',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'لوگو چهار', 'روف پلن' ),
			'subtitle' => esc_html__( '', 'روف پلن' ),
		),
		array(
			'id'       => 'normal_logo_dimension4',
			'type'     => 'dimensions',
			'title'    => esc_html__( ' ابعاد لوگو', 'روف پلن' ),
			'subtitle' => esc_html__( '', 'روف پلن' ),
			'units'    => array( 'em', 'px', '%' ),
			'default'  => array( 'Width' => '', 'Height' => '' ),
			'required' => array( 'normal_logo_show3', '=', true ),
		),
		array(
			'id'       => 'logo_settings_section_end',
			'type'     => 'section',
			'indent'      => false,
		),
	),
);
