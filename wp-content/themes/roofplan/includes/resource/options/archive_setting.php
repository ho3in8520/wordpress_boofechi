<?php

return array(
	'title'      => esc_html__( 'تنظیمات صفحه آرشیو', 'روف پلن' ),
	'id'         => 'archive_setting',
	'desc'       => '',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'      => 'archive_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سورس آرشیو', 'روف پلن' ),
			'options' => array(
				'd' => esc_html__( 'پیش فرض', 'روف پلن' ),
				'e' => esc_html__( 'المنتور', 'روف پلن' ),
			),
			'default' => 'd',
		),
		array(
			'id'       => 'archive_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'روف پلن' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
				'posts_per_page'=> -1,
			],
			'required' => [ 'archive_source_type', '=', 'e' ],
		),

		array(
			'id'       => 'archive_default_st',
			'type'     => 'section',
			'title'    => esc_html__( 'آرشیو پیش فرض', 'روف پلن' ),
			'indent'   => true,
			'required' => [ 'archive_source_type', '=', 'd' ],
		),
		array(
			'id'      => 'archive_page_banner',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش بنر', 'روف پلن' ),
			'desc'    => esc_html__( 'برای نمایش بنر روی بلاگ فعال کنید', 'روف پلن' ),
			'default' => true,
		),
		array(
			'id'       => 'archive_banner_title',
			'type'     => 'text',
			'title'    => esc_html__( 'عنوان بخش بنر', 'روف پلن' ),
			'desc'     => esc_html__( 'عوان را وارد کنید', 'روف پلن' ),
			'required' => array( 'archive_page_banner', '=', true ),
		),
		array(
			'id'       => 'archive_page_background',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'تصویر پس زمینه', 'روف پلن' ),
			'desc'     => esc_html__( 'تصویر پس زمینه برای بنر را وارد کنید', 'روف پلن' ),
			'default'  => '',
			'required' => array( 'archive_page_banner', '=', true ),
		),

		array(
			'id'       => 'archive_sidebar_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'لایه بندی', 'روف پلن' ),
			'subtitle' => esc_html__( 'محتوای اصلی و چینش سایدبار را انتخاب کنید', 'روف پلن' ),
			'options'  => array(

				'left'  => array(
					'alt' => esc_html__( 'دو ستون چپ', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cl.png',
				),
				'full'  => array(
					'alt' => esc_html__( 'تک ستون', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/1col.png',
				),
				'right' => array(
					'alt' => esc_html__( 'دو ستونه راست', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cr.png',
				),
			),

			'default' => 'right',
		),

		array(
			'id'       => 'archive_page_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'سایدبار', 'روف پلن' ),
			'desc'     => esc_html__( 'سایدبار را برای نمایش در بلاگ لیستی فعال کنید', 'روف پلن' ),
			'required' => array(
				array( 'archive_sidebar_layout', '=', array( 'left', 'right' ) ),
			),
			'options'  => roofplan_get_sidebars(),
		),
		array(
			'id'       => 'archive_default_ed',
			'type'     => 'section',
			'indent'   => false,
			'required' => [ 'archive_source_type', '=', 'd' ],
		),
	),
);





