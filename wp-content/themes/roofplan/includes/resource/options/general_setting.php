<?php
$styles = [];
foreach(range(1, 28) as $val) {
    $styles[$val] = sprintf(esc_html__('استایل %s', 'روف پلن'), $val);
}

return  array(
    'title'      => esc_html__( 'تنظیمات عمومی', 'روف پلن' ),
    'id'         => 'general_setting',
    'desc'       => '',
    'icon'       => 'el el-wrench',
    'fields'     => array(
         array(
            'id' => 'theme_color_scheme',
            'type' => 'color',
            'output' => array('.site-title'),
            'title' => esc_html__('طرح رنگی', 'روف پلن'),
            'default' => '#ed202b',
            'transparent' => false
        ),
		array(
            'id' => 'theme_preloader',
            'type' => 'switch',
            'title' => esc_html__('فعال پیش بار گذار', 'روف پلن'),
            'default' => false,
        ),
		 array(
            'id' => 'to_top',
            'type' => 'switch',
            'title' => esc_html__('پنهان کردن اسکرول به بالا', 'روف پلن'),
            'default' => false,
        ),
		 array(
            'id' => 'theme_rtl',
            'type' => 'switch',
            'title' => esc_html__('انتخاب راست چین', 'روف پلن'),
            'default' => false,
        ),
		
    ),
);
