<?php

return  array(
    'title'      => esc_html__( 'تنظیمات صفحه جستجو', 'روف پلن' ),
    'id'         => 'search_setting',
    'desc'       => '', 
    'subsection' => true,
    'fields'     => array(
	    array(
		    'id'      => 'search_source_type',
		    'type'    => 'button_set',
		    'title'   => esc_html__( 'نوع سورس جستجو', 'روف پلن' ),
		    'options' => array(
			    'd' => esc_html__( 'پیش فرض', 'روف پلن' ),
			    'e' => esc_html__( 'المنتور', 'روف پلن' ),
		    ),
		    'default' => 'd',
	    ),
	    array(
		    'id'       => 'search_elementor_template',
		    'type'     => 'select',
		    'title'    => __( 'قالب', 'روف پلن' ),
		    'data'     => 'posts',
		    'args'     => [
			    'post_type' => [ 'elementor_library' ],
				'posts_per_page'=> -1,
		    ],
		    'required' => [ 'search_source_type', '=', 'e' ],
	    ),

	    array(
		    'id'       => 'search_default_st',
		    'type'     => 'section',
		    'title'    => esc_html__( 'جستجو پیش فرض', 'روف پلن' ),
		    'indent'   => true,
		    'required' => [ 'search_source_type', '=', 'd' ],
	    ),
	    array(
		    'id'      => 'search_page_banner',
		    'type'    => 'switch',
		    'title'   => esc_html__( 'نمایش بنر', 'روف پلن' ),
		    'desc'    => esc_html__( '', 'روف پلن' ),
		    'default' => true,
	    ),
	    array(
		    'id'       => 'search_banner_title',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان بخش بنر', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
		    'required' => array( 'search_page_banner', '=', true ),
	    ),
	    array(
		    'id'       => 'search_page_background',
		    'type'     => 'media',
		    'url'      => true,
		    'title'    => esc_html__( 'تصویر پس زمینه', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
		    'default'  => '',
		    'required' => array( 'search_page_banner', '=', true ),
	    ),

	    array(
		    'id'       => 'search_sidebar_layout',
		    'type'     => 'image_select',
		    'title'    => esc_html__( 'لایه بندی', 'روف پلن' ),
		    'subtitle' => esc_html__( '', 'روف پلن' ),
		    'options'  => array(

			    'left'  => array(
				    'alt' => esc_html__( 'دو ستونه چپ', 'روف پلن' ),
				    'img' => get_template_directory_uri() . '/assets/images/redux/2cl.png',
			    ),
			    'full'  => array(
				    'alt' => esc_html__( 'تک ستونه', 'روف پلن' ),
				    'img' => get_template_directory_uri() . '/assets/images/redux/1col.png',
			    ),
			    'right' => array(
				    'alt' => esc_html__( 'دو ستونه راست', 'روف پلن' ),
				    'img' => get_template_directory_uri() . '/assets/images/redux/2cr.png',
			    ),
		    ),

		    'default' => 'right',
	    ),

	    array(
		    'id'       => 'search_page_sidebar',
		    'type'     => 'select',
		    'title'    => esc_html__( 'سایدبار', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
		    'required' => array(
			    array( 'search_sidebar_layout', '=', array( 'left', 'right' ) ),
		    ),
		    'options'  => roofplan_get_sidebars(),
	    ),
	   //
		array(
			'id'    => 'search_page_title',
			'type'  => 'text',
			'title' => esc_html__( 'عنوان جستجو', 'روف پلن' ),
			'desc'  => esc_html__( '', 'روف پلن' ),

		),
		array(
			'id'    => 'search_page_text',
			'type'  => 'textarea',
			'title' => esc_html__( 'توضیحات صفحه جستجو', 'روف پلن' ),
			'desc'  => esc_html__( '', 'روف پلن' ),

		),
	

		
	    array(
		    'id'       => 'search_default_ed',
		    'type'     => 'section',
		    'indent'   => false,
		    'required' => [ 'search_source_type', '=', 'd' ],
	    ),

    ),
);





