<?php
// search  899
//Header 1
$top_header_show_v1=array(
			'id'      => 'top_header_show_v1',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش نوار بلایی', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
		);
				
$welcome_v1 = array(
		    'id'       => 'welcome_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خوش آمد یک', 'روف پلن' ),
		    'desc'     => esc_html__( 'متن خود را وارد کنید', 'روف پلن' ),
			'default'  => esc_html__( 'به سایت ما خوش آمدید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$country_title_v1= array(
		    'id'       => 'country_title_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان کشور', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ایران', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$country_link_v1=array(
		    'id'       => 'country_link_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک کشور', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$header_text_1 = array(
		    'id'       => 'header_text_1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن هدر یک', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'متن هدر یک', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$header_text_link_1=array(
		    'id'       => 'header_text_link_1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک متن هدر یک', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$header_text_2 = array(
		    'id'       => 'header_text_2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن هدر ۲', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'متن هدر ۲', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$header_text_link_2=array(
		    'id'       => 'header_text_link_2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک متن هدر ۲', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$header_text_3 = array(
		    'id'       => 'header_text_3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن هدر ۳', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'متن هدر سه', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$header_text_link_3=array(
		    'id'       => 'header_text_link_3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک متن هدر ۳', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$header_text_4 = array(
		    'id'       => 'header_text_4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن هدر ۴', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'متن هدر ۴', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$header_text_link_4=array(
		    'id'       => 'header_text_link_4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک متن هدر ۴', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$about_v1 = array(
		    'id'       => 'about_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن درباره ما', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ما بهترین هستیم', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );		

$email_title_v1= array(
		    'id'       => 'email_title_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ایمیل بفرستید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$search_title_v1= array(
	'id'       => 'search_title_v1',
	'type'     => 'text',
	'title'    => esc_html__( 'عنوان جستجو', 'روف پلن' ),
	'desc'     => esc_html__( '', 'روف پلن' ),
	'default'  => esc_html__( 'جستجو', 'روف پلن' ),
	'required' => array( 'header_style_settings', '=', 'header_v1' ),
);
$email_v1= array(
		    'id'       => 'email_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'آدرس ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ایمیل بفرستید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$address_title_v1= array(
		    'id'       => 'address_title_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'آدرس ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$address_v1= array(
		    'id'       => 'address_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'اولین خط آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'آدرس ', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$address2nd_v1= array(
		    'id'       => 'address2nd_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'دومین خط آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'آدرس', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$phone_title_v1= array(
		    'id'       => 'phone_title_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'تماس بگیرید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$phone_v1= array(
		    'id'       => 'phone_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'شماره تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '۰۹۱۲۱۲۳۴۵۶۷', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$time_title_v1 = array(
		    'id'       => 'time_title_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان زمان', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ساعات کاری', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$open_time_v1= array(
		    'id'       => 'open_time_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'ساعت کاری', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'هر روز از ۸ الی ۲۰', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$close_time_v1= array(
		    'id'       => 'close_time_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'زمان تعطیلی', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'جمعه ها تعطیل', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$quote_show_v1=array(
			'id'      => 'quote_show_v1',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش نقل قول', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
		);
$quote_v1= array(
		    'id'       => 'quote_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان نقل قول', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '۳', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
	
$quote_link_v1=array(
		    'id'       => 'quote_link_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک نقل قول', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$button_show_v1=array(
			'id'      => 'button_show_v1',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دکمه', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
		);
$button_v1= array(
		    'id'       => 'button_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دکمه', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ثبت نام', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$button_link_v1=array(
		    'id'       => 'button_link_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک دکمه', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );

$login_show_v1=array(
			'id'      => 'login_show_v1',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش ورود', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
		);
$login_v1= array(
		    'id'       => 'login_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ورود', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ورود به حساب', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$login_link_v1=array(
		    'id'       => 'login_link_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک ورود', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$download_show_v1=array(
			'id'      => 'download_show_v1',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دانلود', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
		);
$download_v1= array(
		    'id'       => 'download_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دانلود', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'دانلود', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$download_link_v1=array(
		    'id'       => 'download_link_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'دانلود لینک', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );			
		
$header_social_show_v1=array(
			'id'      => 'header_social_show_v1',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش آیکون شبکه اجتماعی', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
		);
$socialtitle_v1	= array(
		    'id'       => 'socialtitle_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان شبکه اجتماعی', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'اشتراک گذاری', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$header_social_v1= array(
			'id'    => 'header_social_v1',
			'type'  => 'social_media',
			'title' => esc_html__( 'شبکه های اجتماعی', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
		);
$shop_show_v1=array(
			'id'      => 'shop_show_v1',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دکمه فروشگاه', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
		);
$shop_link_v1=array(
		    'id'       => 'shop_link_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک فروشگاه', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$header_search_show_v1=array(
			'id'      => 'header_search_show_v1',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش جستجو', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
		);
$search_text_v1= array(
		    'id'       => 'search_text_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان جستجو', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'جستجو', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );	



$phone_link_v1=array(
		    'id'       => 'phone_link_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$email_link_v1=array(
		    'id'       => 'email_link_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
$search_title_v1= array(
		    'id'       => 'search_title_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان جستجو', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'جستجو های اخیر', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );
	
$cart_link_v1=array(
		    'id'       => 'cart_link_v1',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک جستجو', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
	    );



		

//Header Style 2 area============
//Header 2
$top_header_show_v2=array(
			'id'      => 'top_header_show_v2',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش نوار', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
		);
				
$welcome_v2 = array(
		    'id'       => 'welcome_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خوش آمد', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'قدم رنجه فرمودین', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$about_v2 = array(
		    'id'       => 'about_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن درباره ما', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ما خلی خوبیم!', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );		

$email_title_v2= array(
		    'id'       => 'email_title_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ایمیل بفرستید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$email_v2= array(
		    'id'       => 'email_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'آدرس ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ایمیل بفرستید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$address_title_v2= array(
		    'id'       => 'address_title_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'آدرس ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$address_v2= array(
		    'id'       => 'address_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خط اول آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( ' آدرس', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$address2nd_v2= array(
		    'id'       => 'address2nd_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خط دوم آرس.', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'Sydney Australia', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$phone_title_v2= array(
		    'id'       => 'phone_title_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'تماس بگیرید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$phone_v2= array(
		    'id'       => 'phone_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'شماره تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '۰۹۱۲۱۲۳۴۵۵۶', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$time_title_v2 = array(
		    'id'       => 'time_title_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ساعت کاری', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ساعات کاری ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$open_time_v2= array(
		    'id'       => 'open_time_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'ساعت کاری', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'هر روز ۸ الی ۲۰', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$close_time_v2= array(
		    'id'       => 'close_time_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'زمان تعطیلی', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'جمعه ها تعطیل', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$quote_show_v2=array(
			'id'      => 'quote_show_v2',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش نقل قول', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
		);
$quote_v2= array(
		    'id'       => 'quote_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان نقل قول', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'نقل قول', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
	
$quote_link_v2=array(
		    'id'       => 'quote_link_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک نقل قول', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$button_show_v2=array(
			'id'      => 'button_show_v2',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دکمه', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
		);
$button_v2= array(
		    'id'       => 'button_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دکمه', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ثبت نام', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$button_link_v2=array(
		    'id'       => 'button_link_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک دکمه', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );

$login_show_v2=array(
			'id'      => 'login_show_v2',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش ورود', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
		);
$login_v2= array(
		    'id'       => 'login_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ورود', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ورود به حساب', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$login_link_v2=array(
		    'id'       => 'login_link_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک ورود به حساب', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$download_show_v2=array(
			'id'      => 'download_show_v2',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دانلود', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
		);
$download_v2= array(
		    'id'       => 'download_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دانلود', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'دانلود', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$download_link_v2=array(
		    'id'       => 'download_link_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دانلود', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );			
		
$header_social_show_v2=array(
			'id'      => 'header_social_show_v2',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش آیکون شبکه اجتماعی', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
		);
$socialtitle_v2	= array(
		    'id'       => 'socialtitle_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان شبکه اجتماعی', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'اشتراک گذاری', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$header_social_v2= array(
			'id'    => 'header_social_v2',
			'type'  => 'social_media',
			'title' => esc_html__( 'شبکه های اجتماعی', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
		);
$shop_show_v2=array(
			'id'      => 'shop_show_v2',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دکمه فروشگاه', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
		);
$shop_link_v2=array(
		    'id'       => 'shop_link_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک فروشگاه', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$header_search_show_v2=array(
			'id'      => 'header_search_show_v2',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش جستجو', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
		);
$search_text_v2= array(
		    'id'       => 'search_text_v2',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان جستجو', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'جستجو', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );	
//sidebar//	
	
	
$about_title= array(
		    'id'       => 'about_title',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان درباره ما', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'عنوان درباره ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$about_text= array(
		    'id'       => 'about_text',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن درباره ما', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'متن درباره ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$contact_title= array(
		    'id'       => 'contact_title',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان تماس با ما', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'عنوان تماس با ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$address_title= array(
		    'id'       => 'address_title',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'عنوان آدرس', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$address_text= array(
		    'id'       => 'address_text',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'متن آدرس', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$phone_title= array(
		    'id'       => 'phone_title',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'عنوان تلفن', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$phone_number= array(
		    'id'       => 'phone_number',
		    'type'     => 'text',
		    'title'    => esc_html__( 'شماره تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'شماره تلفن', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$email_title= array(
		    'id'       => 'email_title',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'عنوان ایمیل', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$email_address= array(
		    'id'       => 'email_address',
		    'type'     => 'text',
		    'title'    => esc_html__( 'آدرس ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( 'آدرس ایمیل', 'روف پلن' ),
			'default'  => esc_html__( 'آدرس ایمیل', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$subscribe_title= array(
		    'id'       => 'subscribe_title',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان عضویت', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'عنوان عضویت', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$subscribe_text= array(
		    'id'       => 'subscribe_text',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن عضویت', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'متن عضویت', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );
$copy_right_text= array(
		    'id'       => 'copy_right_text',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن کپی رایت', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'متن کپی رایت', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
	    );









		
//Header 1
$top_header_show_v3=array(
			'id'      => 'top_header_show_v3',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش هدر ۲', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
		);
				
$welcome_v3 = array(
		    'id'       => 'welcome_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خوش آمد ۱', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'خیلی خوش اومدید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$about_v3 = array(
		    'id'       => 'about_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن درباره ما', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ما خلی خوبیم!', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );		

$email_title_v3= array(
		    'id'       => 'email_title_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ایمیل بفرستید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$email_v3= array(
		    'id'       => 'email_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'آدرس ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ایمیل بفرستید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$address_title_v3= array(
		    'id'       => 'address_title_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'آدرس ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$address_v3= array(
		    'id'       => 'address_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خط اول آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( ' آدرس', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$address2nd_v3= array(
		    'id'       => 'address2nd_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خط دوم آرس.', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'Sydney Australia', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$phone_title_v3= array(
		    'id'       => 'phone_title_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'تماس بگیرید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$phone_v3= array(
		    'id'       => 'phone_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'شماره تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '۰۹۱۲۱۲۳۴۵۵۶', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$time_title_v3 = array(
		    'id'       => 'time_title_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ساعت کاری', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ساعات کاری ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$open_time_v3= array(
		    'id'       => 'open_time_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'ساعت کاری', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'هر روز ۸ الی ۲۰', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$close_time_v3= array(
		    'id'       => 'close_time_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'زمان تعطیلی', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'جمعه ها تعطیل', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$quote_show_v3=array(
			'id'      => 'quote_show_v3',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش نقل قول', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
		);
$quote_v3= array(
		    'id'       => 'quote_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان نقل قول', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'نقل قول', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
	
$quote_link_v3=array(
		    'id'       => 'quote_link_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک نقل قول', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$button_show_v3=array(
			'id'      => 'button_show_v3',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دکمه', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
		);
$button_v3= array(
		    'id'       => 'button_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دکمه', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'ثبت نام', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$button_link_v3=array(
		    'id'       => 'button_link_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک دکمه', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );

$login_show_v3=array(
			'id'      => 'login_show_v3',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش ورود', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
		);
$login_v3= array(
		    'id'       => 'login_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ورود', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'ورود', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$login_link_v3=array(
		    'id'       => 'login_link_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک ورود به حساب', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$download_show_v3=array(
			'id'      => 'download_show_v3',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دانلود', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
		);
$download_v3= array(
		    'id'       => 'download_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دانلود', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'دانلود', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$download_link_v3=array(
		    'id'       => 'download_link_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دانلود', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );			
		
$header_social_show_v3=array(
			'id'      => 'header_social_show_v3',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش آیکون شبکه اجتماعی', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
		);
$socialtitle_v3	= array(
		    'id'       => 'socialtitle_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان شبکه اجتماعی', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'اشتراک گذاری', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$header_social_v3= array(
			'id'    => 'header_social_v3',
			'type'  => 'social_media',
			'title' => esc_html__( 'شبکه های اجتماعی', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
		);
$shop_show_v3=array(
			'id'      => 'shop_show_v3',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دکمه فروشگاه', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
		);
$shop_link_v3=array(
		    'id'       => 'shop_link_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک فروشگاه', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$header_search_show_v3=array(
			'id'      => 'header_search_show_v3',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش جستجو', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
		);
$search_text_v3= array(
		    'id'       => 'search_text_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان جستجو', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'جستجو', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
		
$country_title_v3= array(
		    'id'       => 'country_title_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( ' عنوان کشور', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ایران', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$country_link_v3=array(
		    'id'       => 'country_link_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک کشور', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
$email_link_v3=array(
		    'id'       => 'email_link_v3',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
	    );
		
		
		
		
		
		
//Header 4
$top_header_show_v4=array(
			'id'      => 'top_header_show_v4',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش نوار بالای', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
		);
				
$welcome_v4 = array(
		    'id'       => 'welcome_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خوش آمد', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( ' خوش آمدید!', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$about_v4 = array(
		    'id'       => 'about_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن درباره ما', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'ما خلی خوبیم!', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );		

$email_title_v4= array(
		    'id'       => 'email_title_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '  ', 'روف پلن' ),
			'default'  => esc_html__( 'ایمیل بفرستید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$email_v4= array(
		    'id'       => 'email_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'آدرس ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ایمیل بفرستید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$address_title_v4= array(
		    'id'       => 'address_title_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'آدرس ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$address_v4= array(
		    'id'       => 'address_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خط اول آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( ' آدرس', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$address2nd_v4= array(
		    'id'       => 'address2nd_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خط دوم آرس.', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'Sydney Australia', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$phone_title_v4= array(
		    'id'       => 'phone_title_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'تماس بگیرید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$phone_v4= array(
		    'id'       => 'phone_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'شماره تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( '۰۹۱۲۱۲۳۴۵۵۶', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$time_title_v4 = array(
		    'id'       => 'time_title_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ساعت کاری', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( 'ساعات کاری ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$open_time_v4= array(
		    'id'       => 'open_time_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'ساعت کاری', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( 'هر روز ۸ الی ۲۰', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$close_time_v4= array(
		    'id'       => 'close_time_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'زمان تعطیلی', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( 'جمعه ها تعطیل', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$quote_show_v4=array(
			'id'      => 'quote_show_v4',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش نقل قول', 'روف پلن' ),
			'desc'    => esc_html__( '   ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
		);
$quote_v4= array(
		    'id'       => 'quote_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان نقل قول', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( 'نقل قول', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
	
$quote_link_v4=array(
		    'id'       => 'quote_link_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک نقل قول', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$button_show_v4=array(
			'id'      => 'button_show_v4',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دکمه', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
		);
$button_v4= array(
		    'id'       => 'button_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دکمه', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'ثبت نام', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$button_link_v4=array(
		    'id'       => 'button_link_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک دکمه', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );

$login_show_v4=array(
			'id'      => 'login_show_v4',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش ورود', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
		);
$login_v4= array(
		    'id'       => 'login_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ورود', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'Login', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$login_link_v4=array(
		    'id'       => 'login_link_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک ورود به حساب', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$download_show_v4=array(
			'id'      => 'download_show_v4',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دانلود', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
		);
$download_v4= array(
		    'id'       => 'download_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دانلود', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'Download', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$download_link_v4=array(
		    'id'       => 'download_link_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دانلود', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );			
		
$header_social_show_v4=array(
			'id'      => 'header_social_show_v4',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش آیکون شبکه اجتماعی', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
		);
$socialtitle_v4	= array(
		    'id'       => 'socialtitle_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان شبکه اجتماعی', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'اشتراک گذاری', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$header_social_v4= array(
			'id'    => 'header_social_v4',
			'type'  => 'social_media',
			'title' => esc_html__( 'شبکه های اجتماعی', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
		);
$shop_show_v4=array(
			'id'      => 'shop_show_v4',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دکمه فروشگاه', 'روف پلن' ),
			'desc'    => esc_html__( '  فروشگاه', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
		);
$shop_link_v4=array(
		    'id'       => 'shop_link_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک فروشگاه', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
$header_search_show_v4=array(
			'id'      => 'header_search_show_v4',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش جستجو', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
		);
$search_text_v4= array(
		    'id'       => 'search_text_v4',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان جستجو', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'جستجو', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
	    );
//Header 1
$top_header_show_v5=array(
			'id'      => 'top_header_show_v5',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش نوار بالای', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
		);
				
$welcome_v5 = array(
		    'id'       => 'welcome_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خوش آمد', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( ' خوش آمدید!', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$about_v5 = array(
		    'id'       => 'about_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن درباره ما', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ما خلی خوبیم!', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );		

$email_title_v5= array(
		    'id'       => 'email_title_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( 'ایمیل بفرستید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$email_v5= array(
		    'id'       => 'email_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'آدرس ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( 'Enter آدرس ایمیل', 'روف پلن' ),
			'default'  => esc_html__( 'ایمیل بفرستید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$address_title_v5= array(
		    'id'       => 'address_title_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( 'Enter عنوان آدرس', 'روف پلن' ),
			'default'  => esc_html__( 'آدرس ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$address_v5= array(
		    'id'       => 'address_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خط اول آدرس', 'روف پلن' ),
		    'desc'     => esc_html__( 'Enter خط اول آدرس', 'روف پلن' ),
			'default'  => esc_html__( ' آدرس', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$address2nd_v5= array(
		    'id'       => 'address2nd_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'خط دوم آرس.', 'روف پلن' ),
		    'desc'     => esc_html__( 'Enter خط دوم آرس.', 'روف پلن' ),
			'default'  => esc_html__( 'Sydney Australia', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$phone_title_v5= array(
		    'id'       => 'phone_title_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( 'Enter Text of Phone', 'روف پلن' ),
			'default'  => esc_html__( 'تماس بگیرید', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$phone_v5= array(
		    'id'       => 'phone_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'شماره تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( '۰۹۱۲۱۲۳۴۵۵۶', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$time_title_v5 = array(
		    'id'       => 'time_title_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ساعت کاری', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( 'ساعات کاری ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$open_time_v5= array(
		    'id'       => 'open_time_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'ساعت کاری', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( 'هر روز ۸ الی ۲۰', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$close_time_v5= array(
		    'id'       => 'close_time_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'زمان تعطیلی', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( 'جمعه ها تعطیل', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$quote_show_v5=array(
			'id'      => 'quote_show_v5',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش نقل قول', 'روف پلن' ),
			'desc'    => esc_html__( '   ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
		);
$quote_v5= array(
		    'id'       => 'quote_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان نقل قول', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( 'نقل قول', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
	
$quote_link_v5=array(
		    'id'       => 'quote_link_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک نقل قول', 'روف پلن' ),
		    'desc'     => esc_html__( '   ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$button_show_v5=array(
			'id'      => 'button_show_v5',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دکمه', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
		);
$button_v5= array(
		    'id'       => 'button_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دکمه', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'ثبت نام', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$button_link_v5=array(
		    'id'       => 'button_link_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک دکمه', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );

$login_show_v5=array(
			'id'      => 'login_show_v5',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش ورود', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
		);
$login_v5= array(
		    'id'       => 'login_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان ورود', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'Login', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$login_link_v5=array(
		    'id'       => 'login_link_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک ورود به حساب', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$download_show_v5=array(
			'id'      => 'download_show_v5',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دانلود', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
		);
$download_v5= array(
		    'id'       => 'download_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دانلود', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'Download', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$download_link_v5=array(
		    'id'       => 'download_link_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان دانلود', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );			
		
$header_social_show_v5=array(
			'id'      => 'header_social_show_v5',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش آیکون شبکه اجتماعی', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
		);
$socialtitle_v5	= array(
		    'id'       => 'socialtitle_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان شبکه اجتماعی', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'اشتراک گذاری', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$header_social_v5= array(
			'id'    => 'header_social_v5',
			'type'  => 'social_media',
			'title' => esc_html__( 'شبکه های اجتماعی', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
		);
$shop_show_v5=array(
			'id'      => 'shop_show_v5',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دکمه فروشگاه', 'روف پلن' ),
			'desc'    => esc_html__( ' ', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
		);
$shop_link_v5=array(
		    'id'       => 'shop_link_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک فروشگاه', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$header_search_show_v5=array(
			'id'      => 'header_search_show_v5',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش جستجو', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
		);
$search_text_v5= array(
		    'id'       => 'search_text_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان جستجو', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'جستجو', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );			

$header3_image1 = array(
			'id'       => 'header3_image1',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۱', 'روف پلن' ),
		);
$header3_image2 =array(
			'id'       => 'header3_image2',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۲', 'روف پلن' ),
		);
$header3_image3 =array(
			'id'       => 'header3_image3',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۳', 'روف پلن' ),
		);	
$header3_image4 =array(
			'id'       => 'header3_image4',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۴ عکس ۴', 'روف پلن' ),
		);
$header3_image5 =array(
			'id'       => 'header3_image5',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۵', 'روف پلن' ),
		);
$header3_image6 =array(
			'id'       => 'header3_image6',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۶', 'روف پلن' ),
		);
$header3_image7 =array(
			'id'       => 'header3_image7',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۷', 'روف پلن' ),
		);		
//
$header2_image1 = array(
			'id'       => 'header2_image1',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۱', 'روف پلن' ),
		);
$header2_image2 =array(
			'id'       => 'header2_image2',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر۳ عکس ۲', 'روف پلن' ),
		);
$header2_image3 =array(
			'id'       => 'header2_image3',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۳', 'روف پلن' ),
		);	
$header2_image4 =array(
			'id'       => 'header2_image4',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۴', 'روف پلن' ),
		);
$header2_image5 =array(
			'id'       => 'header2_image5',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۵', 'روف پلن' ),
		);
//
$header1_image1 = array(
			'id'       => 'header1_image1',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۱', 'روف پلن' ),
		);
$header1_image2 =array(
			'id'       => 'header1_image2',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۲', 'روف پلن' ),
		);
$header1_image3 =array(
			'id'       => 'header1_image3',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۳', 'روف پلن' ),
		);	
$header1_image4 =array(
			'id'       => 'header1_image4',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۴', 'روف پلن' ),
		);
$header1_image5 =array(
			'id'       => 'header1_image5',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'هدر ۳ عکس ۵', 'روف پلن' ),
		);		
		
		
		
$about_title_v5= array(
		    'id'       => 'about_title_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'عنوان درباره ما', 'روف پلن' ),
		    'desc'     => esc_html__( ' ', 'روف پلن' ),
			'default'  => esc_html__( 'درباره', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$about_text_v5= array(
		    'id'       => 'about_text_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'متن درباره ما', 'روف پلن' ),
		    'desc'     => esc_html__( '  ', 'روف پلن' ),
			'default'  => esc_html__( 'درباره ما', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$phone_link_v5=array(
		    'id'       => 'phone_link_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( '  لینک تلفن', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );
$email_link_v5=array(
		    'id'       => 'email_link_v5',
		    'type'     => 'text',
		    'title'    => esc_html__( 'لینک ایمیل', 'روف پلن' ),
		    'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => esc_html__( '#', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
	    );		
		
		
		
//PreDefine Area End==================

// Main Area 899
return array(
	'title'      => esc_html__( 'تنظیمات سربرگ', 'روف پلن' ),
	'id'         => 'headers_setting',
	'desc'       => '',
	'subsection' => false,
	'fields'     => array(
		array(
			'id'      => 'header_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سورس سربرگ', 'روف پلن' ),
			'options' => array(
				'd' => esc_html__( 'پیشفرض', 'روف پلن' ),
				'e' => esc_html__( 'المنتور', 'روف پلن' ),
			),
			'default' => 'd',
		),
		array(
			'id'       => 'header_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'روف پلن' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
				'posts_per_page'	=> -1
			],
			'required' => [ 'header_source_type', '=', 'e' ],
		),
		array(
			'id'       => 'header_style_section_start',
			'type'     => 'section',
			'indent'      => true,
			'title'    => esc_html__( 'تنظیمات هدر', 'روف پلن' ),
			'required' => array( 'header_source_type', '=', 'd' ),
		),
		array(
		    'id'       => 'header_style_settings',
		    'type'     => 'image_select',
		    'title'    => esc_html__( 'انتخاب استایل هدر', 'روف پلن' ),
		    'subtitle' => esc_html__( 'انتخاب استایل هدر', 'روف پلن' ),
		    'options'  => array(

			    'header_v1'  => array(
				    'alt' => esc_html__( 'استایل هدر یک', 'روف پلن' ),
				    'img' => get_template_directory_uri() . '/assets/images/redux/header/header1.png',
			    ),
			    'header_v2'  => array(
				    'alt' => esc_html__( 'استایل هدر دو', 'روف پلن' ),
				    'img' => get_template_directory_uri() . '/assets/images/redux/header/header2.png',
			    ),
				  'header_v3'  => array(
				    'alt' => esc_html__( 'استایل هدر سه', 'روف پلن' ),
				    'img' => get_template_directory_uri() . '/assets/images/redux/header/header3.png',
			    ),
			
				  'header_v5'  => array(
				    'alt' => esc_html__( 'استایل هدر پنج', 'روف پلن' ),
				    'img' => get_template_directory_uri() . '/assets/images/redux/header/header5.png',
			    ),
				'header_v4'  => array(
				    'alt' => esc_html__( 'استایل هدر چهار', 'روف پلن' ),
				    'img' => get_template_directory_uri() . '/assets/images/redux/header/onepage.png',
			    ),
			),
			'required' => array( 'header_source_type', '=', 'd' ),
			'default' => 'header_v1',
	    ),			
		array(
			'id'       => 'header_v1_settings_section_start',
			'type'     => 'section',
			'indent'      => true,
			'title'    => esc_html__( 'تنظیمات استایل هدر یک', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v1' ),
		),
		
		//Welcome
			$top_header_show_v1,
			//$welcome_v1,
			//$about_v1,
			//$button_v1,
		//Header Social	
			//$header_social_show_v1,
			$socialtitle_v1,
			$header_social_v1,
		//Country
			$country_title_v1,
			$country_link_v1,
		//Email	
			//$email_title_v1,
			$email_v1,
			$email_link_v1,
		//login	
			//$login_show_v1,
			//$login_v1,
			//$login_link_v1,
		//Address	
			//$address_title_v1,
			//$address_v1,
			//$address2nd_v1,
		//Phone	
			//$phone_title_v1,
			//$phone_v1,
		//Shop			
			$shop_show_v1,
			//$shop_link_v1,
		//Quote	
			//$quote_show_v1,
			//$quote_v1,
			//$quote_link_v1,
		//Time	
			//$time_title_v1,
			//$open_time_v1,
		//Button	
			$button_show_v1,
			$button_v1,
			$button_link_v1,
		//login	
			//$login_show_v1,
			//$login_v1,
			//$login_link_v1,
		//download	
			//$download_show_v1,
			//$download_v1,
			//$download_link_v1,
		
		//Search
			$header_search_show_v1,
			$search_text_v1,
			//$search_title_v1,
		
		
		
			
		/***********************************************************************
								Header Version 2 Start
		************************************************************************/
		array(
			'id'       => 'header_v2_settings_section_start',
			'type'     => 'section',
			'indent'      => true,
			'title'    => esc_html__( 'تنظیمات هدر استایل دو', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v2' ),
		),
		//Welcome
			//$top_header_show_v2,
			//$welcome_v2,
			//$about_v2,
		//Address	
			//$address_title_v2,
			//$address_v2,
			//$address2nd_v2,	
		//Email	
			//$email_title_v2,
			//$email_v2,
		//Time	
			//$time_title_v2,
		//login	
			//$login_show_v2,
			//$login_v2,
			//$login_link_v2,
		//Search
			$header_search_show_v2,
			//$search_text_v2,
		//Shop			
			$shop_show_v2,
			//$shop_link_v2,
		//Quote	
			//$quote_show_v2,
			//$quote_v2,
			//$quote_link_v2,
		//Button	
			$button_show_v2,
			$button_v2,
			$button_link_v2,
		//Header Social	
			$header_social_show_v2,
			//$socialtitle_v2,
			$header_social_v2,
		//Phone	
			//$phone_title_v2,
			//$phone_v2,
		//About	
			//$about_title,
			//$about_text,
		//Contact	
			//$contact_title,
			//$address_title,
			//$address_text,
		//Phone	
			//$phone_title,
			//$phone_number,
		//Email	
			//$email_title,
			//$email_address,
		//Subscribe	
			//$subscribe_title,
			//$subscribe_text,
		//Copy Right Text
			//$copy_right_text,
		
		
		
		/***********************************************************************
								Header Version 3 Start
		************************************************************************/
		array(
			'id'       => 'header_v3_settings_section_start',
			'type'     => 'section',
			'indent'      => true,
			'title'    => esc_html__( 'تنظیمات استایل هدر سه', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v3' ),
		),
		//Welcome
			$top_header_show_v3,
			//$welcome_v3,
			//$about_v3,
			//$button_v3,
		//Header Social	
			$header_social_show_v3,
			$socialtitle_v3,
			$header_social_v3,
		
		//Country
			$country_title_v3,
			$country_link_v3,
		//Email	
			//$email_title_v3,
			$email_v3,
			$email_link_v3,
		//login	
			//$login_show_v3,
			//$login_v3,
			//$login_link_v3,
		//Address	
			//$address_title_v3,
			//$address_v3,
			//$address2nd_v3,
		//Phone	
			//$phone_title_v3,
			//$phone_v3,
		//Shop			
			$shop_show_v3,
			//$shop_link_v3,
		//Quote	
			//$quote_show_v3,
			//$quote_v3,
			//$quote_link_v3,
		//Time	
			//$time_title_v3,
			//$open_time_v3,
		//Button	
			$button_show_v3,
			$button_v3,
			$button_link_v3,
		//login	
			//$login_show_v3,
			//$login_v3,
			//$login_link_v3,
		//download	
			//$download_show_v3,
			//$download_v3,
			//$download_link_v3,
		
		//Search
			$header_search_show_v3,
			//$search_text_v3,
			//$search_title_v3,
			
	/***********************************************************************
								Header Version 4 Start
		************************************************************************/
		array(
			'id'       => 'header_v4_settings_section_start',
			'type'     => 'section',
			'indent'      => true,
			'title'    => esc_html__( 'تنظیمات استایل هدر چهار', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v4' ),
		),
		//Welcome
			//$top_header_show_v2,
			//$welcome_v2,
			//$about_v2,
		//Address	
			//$address_title_v2,
			//$address_v2,
			//$address2nd_v2,	
		//Email	
			//$email_title_v2,
			//$email_v2,
		//Time	
			//$time_title_v2,
		//login	
			//$login_show_v2,
			//$login_v2,
			//$login_link_v2,
		//Search
			$header_search_show_v4,
			//$search_text_v2,
		//Shop			
			$shop_show_v4,
			//$shop_link_v2,
		//Quote	
			//$quote_show_v2,
			//$quote_v2,
			//$quote_link_v2,
		//Button	
			$button_show_v4,
			$button_v4,
			$button_link_v4,
		//Header Social	
			$header_social_show_v4,
			//$socialtitle_v2,
			$header_social_v4,
		//Phone	
			//$phone_title_v2,
			//$phone_v2,
		//About	
			//$about_title,
			//$about_text,
		//Contact	
			//$contact_title,
			//$address_title,
			//$address_text,
		//Phone	
			//$phone_title,
			//$phone_number,
		//Email	
			//$email_title,
			//$email_address,
		//Subscribe	
			//$subscribe_title,
			//$subscribe_text,
		//Copy Right Text
			//$copy_right_text,
		/***********************************************************************
								Header Version 5 Start
		************************************************************************/
		array(
			'id'       => 'header_v5_settings_section_start',
			'type'     => 'section',
			'indent'      => true,
			'title'    => esc_html__( 'تنظیمات استایل هدر پنج', 'روف پلن' ),
			'required' => array( 'header_style_settings', '=', 'header_v5' ),
		),
		//About	
			$about_title_v5,
			$about_text_v5,
		//Address	
			$address_title_v5,
			$address_v5,
			//$address2nd_v5,			
		//Phone	
			$phone_title_v5,
			$phone_v5,
			$phone_link_v5,
		//Email	
			//$email_title_v5,
			$email_v5,
			$email_link_v5,
		
		//Header Social	
			//$header_social_show_v5,
			$socialtitle_v5,
			$header_social_v5,
		//Quote	
			//$quote_show_v5,
			//$quote_v5,
			//$quote_link_v5,
		
		//login	
			//$login_show_v5,
			//$login_v5,
			//$login_link_v5,
		//download	
			//$download_show_v5,
			//$download_v5,
			//$download_link_v5,	
		
		
		//Welcome
			$top_header_show_v5,
			//$welcome_v5,
			//$about_v5,
		//Time	
			$time_title_v5,
			//$open_time_v5,
			//$close_time_v5,
		//Search
			$header_search_show_v5,
			//$search_text_v5, 
		//Shop			
			$shop_show_v5,
			//$shop_link_v5,
		//Button	
			$button_show_v5,
			$button_v5,
			$button_link_v5,
		
		array(
			'id'       => 'header_style_section_end',
			'type'     => 'section',
			'indent'      => false,
			'required' => [ 'header_source_type', '=', 'd' ],
		),
		
		
		
		
	/***** ==========Main Area End======== */	

	
	
	
	
	
	
	
	),
);
