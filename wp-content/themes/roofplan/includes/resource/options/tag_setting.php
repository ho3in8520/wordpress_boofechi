<?php

return array(
	'title'      => esc_html__( 'تنظیمات صفحه برچسب', 'روف پلن' ),
	'id'         => 'tag_setting',
	'desc'       => '',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'      => 'tag_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سورس برچسب', 'روف پلن' ),
			'options' => array(
				'd' => esc_html__( 'پیش فرض', 'روف پلن' ),
				'e' => esc_html__( 'المنتور', 'روف پلن' ),
			),
			'default' => 'd',
		),
		array(
			'id'       => 'tag_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'روف پلن' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
				'posts_per_page'=> -1,
			],
			'required' => [ 'tag_source_type', '=', 'e' ],
		),

		array(
			'id'       => 'tag_default_st',
			'type'     => 'section',
			'title'    => esc_html__( 'پیش فرض برچسب', 'روف پلن' ),
			'indent'   => true,
			'required' => [ 'tag_source_type', '=', 'd' ],
		),
		array(
			'id'      => 'tag_page_banner',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش بنر', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
		),
		array(
			'id'       => 'tag_banner_title',
			'type'     => 'text',
			'title'    => esc_html__( 'عنوان بخش بنر', 'روف پلن' ),
			'desc'     => esc_html__( '', 'روف پلن' ),
			'required' => array( 'tag_page_banner', '=', true ),
		),
		array(
			'id'       => 'tag_page_background',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'تصویر پس زمینه', 'روف پلن' ),
			'desc'     => esc_html__( '', 'روف پلن' ),
			'default'  => '',
			'required' => array( 'tag_page_banner', '=', true ),
		),

		array(
			'id'       => 'tag_sidebar_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'لایه بندی', 'روف پلن' ),
			'subtitle' => esc_html__( '', 'روف پلن' ),
			'options'  => array(

				'left'  => array(
					'alt' => esc_html__( 'دو ستونه چپ', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cl.png',
				),
				'full'  => array(
					'alt' => esc_html__( 'تک ستونه', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/1col.png',
				),
				'right' => array(
					'alt' => esc_html__( 'دو ستونه راست', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cr.png',
				),
			),

			'default' => 'right',
		),

		array(
			'id'       => 'tag_page_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'سایدبار', 'روف پلن' ),
			'desc'     => esc_html__( '', 'روف پلن' ),
			'required' => array(
				array( 'tag_sidebar_layout', '=', array( 'left', 'right' ) ),
			),
			'options'  => roofplan_get_sidebars(),
		),
		array(
			'id'       => 'tag_default_ed',
			'type'     => 'section',
			'indent'   => false,
			'required' => [ 'tag_source_type', '=', 'd' ],
		),
	),
);





