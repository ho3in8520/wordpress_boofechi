<?php
return array(
	'title'      => esc_html__( 'تنظیمات سربرگ دو', 'روف پلن' ),
	'id'         => 'header2_setting',
	'desc'       => '',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'      => 'logo_type2',
			'type'    => 'button_set',
			'title'   => esc_html__( 'استایل لوگو', 'روف پلن' ),
			'desc'    => esc_html__( 'لوگوی خود را با هر مدل برای نمایش در سربرگ انتخاب کنید', 'روف پلن' ),
			'options' => array(
				'image' => esc_html__( 'لوگو تصویر', 'روف پلن' ),
				'text'  => esc_html__( 'لوگو متن', 'روف پلن' ),
			),
			'default' => 'image',
		),
		array(
			'id'       => 'image_logo2',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'لوگو', 'روف پلن' ),
			'subtitle' => esc_html__( 'تصویر لوگوی سایت را با اندازه قابل تنظیم برای قسمت لوگو درج کنید', 'روف پلن' ),
			'default'  => array( 'url' => get_template_directory_uri() . '/assets/images/logo.png' ),
			'required' => array( array( 'logo_type2', 'equals', 'image' ) ),
		),
		array(
			'id'       => 'logo_dimension2',
			'type'     => 'dimensions',
			'title'    => esc_html__( 'ابعاد لوگو', 'روف پلن' ),
			'subtitle' => esc_html__( 'ابعاد لوگو را انتخاب کنید', 'روف پلن' ),
			'units'    => array( 'em', 'px', '%' ),
			'default'  => array( 'Width' => '', 'Height' => '' ),
			'required' => array(
				array( 'logo_type2', 'equals', 'image' ),
			),
		),
		array(
			'id'       => 'logo_text2',
			'type'     => 'text',
			'title'    => esc_html__( 'متن لوگو', 'روف پلن' ),
			'subtitle' => esc_html__( 'متن لوگو را وارد کنید', 'روف پلن' ),
			'required' => array(
				array( 'logo_type2', 'equals', 'text' ),
			),
		),
		array(
			'id'          => 'logo_typography2',
			'type'        => 'typography',
			'title'       => esc_html__( 'تایپوگرافی', 'روف پلن' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
			'line-height' => false,
			'output'      => array( 'h2.site-description' ),
			'units'       => 'px',
			'subtitle'    => esc_html__( 'استایل متن لوگو را انتخاب کنید', 'روف پلن' ),
			'default'     => array(
				'color'       => '#333',
				'font-style'  => '700',
				'font-family' => 'Abel',
				'google'      => true,
				'font-size'   => '33px',
			),
			'required'    => array(
				array( 'logo_type2', 'equals', 'text' ),
			),
		),

		array(
			'id'    => 'header_social_share2',
			'type'  => 'social_media',
			'title' => esc_html__( 'شبکه اجتماعی', 'روف پلن' ),
			'desc'  => esc_html__( 'برای فعال کردن نمادهای شبکه اجتماعی در هدر ، روی یک نماد کلیک کنید.', 'روف پلن' ),
		),
		array(
			'id'    => 'header_project2',
			'type'  => 'text',
			'title' => esc_html__( 'لینک پروژه ها', 'روف پلن' ),
		),
	),
);
