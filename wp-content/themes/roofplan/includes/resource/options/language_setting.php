<?php

return array(

	'title'         => esc_html__( 'تنظیمات زبان', 'roofplan' ),
    'id'            => 'language_settings',
    'desc'          => '',
    'icon'			=> 'el el-globe',
    'fields'        => array(          
		array(
			'id' => 'optLanguage',
			'type' => 'language',
			'desc' => esc_html__('لطفا فایل زبان .mo را آپلود کنید', 'روف پلن'),
			)
	),
);
