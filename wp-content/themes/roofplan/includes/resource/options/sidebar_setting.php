<?php

return array(

    'title'         => esc_html__( 'تنظیمات سفارشی سایدبار', 'روف پلن' ),
    'id'            => 'sidebar_setting',
    'desc'          => '',
    'icon'          => 'el el-indent-left',
    'fields'        => array(
        array(
            'id' => 'custom_sidebar_name',
            'type' => 'multi_text',
            'title' => esc_html__('سایدبار سفارشی پویا', 'روف پلن'),
            'desc' => esc_html__('', 'روف پلن')
            ),
    ),
);

