<?php

return array(
	'title'  => esc_html__( 'تنظیمات صفحه وبلاگ', 'روف پلن' ),
	'id'     => 'blog_setting',
	'desc'   => '',
	'icon'   => 'el el-indent-left',
	'fields' => array(
		array(
			'id'      => 'blog_source_type',
			'type'    => 'button_set',
			'title'   => esc_html__( 'نوع سورس وبلاگ', 'روف پلن' ),
			'options' => array(
				'd' => esc_html__( 'پیش فرض', 'روف پلن' ),
				'e' => esc_html__( 'المنتور', 'روف پلن' ),
			),
			'default' => 'd',
		),
		array(
			'id'       => 'blog_elementor_template',
			'type'     => 'select',
			'title'    => __( 'قالب', 'روف پلن' ),
			'data'     => 'posts',
			'args'     => [
				'post_type' => [ 'elementor_library' ],
				'posts_per_page'=> -1,
			],
			'required' => [ 'blog_source_type', '=', 'e' ],
		),

		array(
			'id'       => 'blog_default_st',
			'type'     => 'section',
			'title'    => esc_html__( 'پیش فرض وبلاگ', 'روف پلن' ),
			'indent'   => true,
			'required' => [ 'blog_source_type', '=', 'd' ],
		),
/*
		array(
			'id'      => 'blog_page_banner',
			'type'    => 'switch',
			'title'   => esc_html__( 'Show Banner', 'روف پلن' ),
			'desc'    => esc_html__( 'Enable to show banner on blog', 'روف پلن' ),
			'default' => true,
		),
		
		array(
			'id'       => 'blog_banner_title',
			'type'     => 'text',
			'title'    => esc_html__( 'Banner Section Title', 'روف پلن' ),
			'desc'     => esc_html__( 'Enter the title to show in banner section', 'روف پلن' ),
			'required' => array( 'blog_page_banner', '=', true ),
		),
		array(
			'id'       => 'blog_page_breadcrumb',
			'type'     => 'raw',
			'content'  => "<div style='background-color:#c33328;color:white;padding:20px;'>" . esc_html__( 'Use Yoast SEO plugin for breadcrumb.', 'روف پلن' ) . "</div>",
			'required' => array( 'blog_page_banner', '=', true ),
		),
		array(
			'id'       => 'blog_page_background',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'Background Image', 'روف پلن' ),
			'desc'     => esc_html__( 'Insert background image for banner', 'روف پلن' ),
			'default'  => array(
				'url' => ROOFPLAN_URI . 'assets/images/top-title-bg.jpg',
			),
			'required' => array( 'blog_page_banner', '=', true ),
		),

		array(
			'id'       => 'blog_sidebar_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Layout', 'روف پلن' ),
			'subtitle' => esc_html__( 'Select main content and sidebar alignment.', 'روف پلن' ),
			'options'  => array(

				'left'  => array(
					'alt' => esc_html__( '2 Column Left', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cl.png',
				),
				'full'  => array(
					'alt' => esc_html__( '1 Column', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/1col.png',
				),
				'right' => array(
					'alt' => esc_html__( '2 Column Right', 'روف پلن' ),
					'img' => get_template_directory_uri() . '/assets/images/redux/2cr.png',
				),
			),

			'default' => 'right',
		),

		array(
			'id'       => 'blog_page_sidebar',
			'type'     => 'select',
			'title'    => esc_html__( 'Sidebar', 'روف پلن' ),
			'desc'     => esc_html__( 'Select sidebar to show at blog listing page', 'روف پلن' ),
			'required' => array(
				array( 'blog_sidebar_layout', '=', array( 'left', 'right' ) ),
			),
			'options'  => روف پلن_get_sidebars(),
		),
*/	
		
		
		array(
			'id'      => 'blog_post_date',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش داده نوشته', 'روف پلن' ),
			'desc'    => esc_html__( 'برای نمایش داده نوشته فعال کنید', 'روف پلن' ),
			'default' => false,
		),
		array(
			'id'      => 'blog_post_catgory',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دسته بندی نوشته', 'روف پلن' ),
			'desc'    => esc_html__( 'برای نمایش دادن فعال کنید', 'روف پلن' ),
			'default' => false,
		),
		array(
			'id'      => 'blog_post_author',
			'type'    => 'switch',
			'title'   => esc_html__( 'Sنمایش نویسنده', 'روف پلن' ),
			'desc'    => esc_html__( 'برای نمایش نویسنده فعال کنید', 'روف پلن' ),
			'default' => false,
		),
			array(
			'id'      => 'blog_post_comment',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش دیدگاه ها', 'روف پلن' ),
			'desc'    => esc_html__( 'برای نمایش تعداد دیگاه ها', 'روف پلن' ),
			'default' => false,
		),
		array(
			'id'      => 'blog_post_readmore',
			'type'    => 'switch',
			'title'   => esc_html__( 'نمایش ادامه مطلب', 'روف پلن' ),
			'desc'    => esc_html__( '', 'روف پلن' ),
			'default' => true,
		),
		
		array(
			'id'       => 'blog_default_ed',
			'type'     => 'section',
			'indent'   => false,
			'required' => [ 'blog_source_type', '=', 'd' ],
		),
	),
);





