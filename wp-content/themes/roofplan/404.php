<?php
/**
 * 404 page file
 *
 * @package    WordPress
 * @subpackage Roofplan
 * @author     template_path
 * @version    1.0
 */

$text = sprintf(__('It seems we can\'t find what you\'re looking for. Perhaps searching can help ', 'roofplan'), esc_url(home_url('/')));
$allowed_html = wp_kses_allowed_html( 'post' );
?>
<?php get_header();
$data = \ROOFPLAN\Includes\Classes\Common::instance()->data( '404' )->get();
do_action( 'roofplan_banner', $data );
$options = roofplan_WSH()->option();

if ( class_exists( '\Elementor\Plugin' ) AND $data->get( 'tpl-type' ) == 'e' AND $data->get( 'tpl-elementor' ) ) {
	echo Elementor\Plugin::instance()->frontend->get_builder_content_for_display( $data->get( 'tpl-elementor' ) );
} else {
	?>
	




<section class="error-page-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="error-content text-center wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
				
					<?php if($options->get('404_page_title' ) ): ?>	
                    <div class="title"><?php echo wp_kses( $options->get( '404_page_title'), $allowed_html ); ?></div>
					<?php else: ?>
					<div class="title"><?php esc_html_e( 'Oops! Page Not Found!', 'roofplan' ); ?></div>
					<?php endif; ?>
					
					<?php if($options->get('404_title' ) ): ?>
                    <div class="big-title"><?php echo wp_kses( $options->get( '404_title'), $allowed_html ); ?></div>
					<?php else: ?>	
					<div class="big-title"><?php esc_html_e( '404', 'roofplan' ); ?></div>
					<?php endif; ?>	
					
					<?php if($options->get('404_page_text' ) ): ?>
                    <p><?php echo wp_kses( $options->get( '404_page_text'), $allowed_html ); ?></p>
					<?php else: ?>	
					<p><?php esc_html_e( 'Sorry we’re unable to find the page you are looking for, please
					check the url and try again.', 'roofplan' ); ?></p>
					<?php endif; ?>	
					
					
					<?php if($options->get('back_home_btn' ) ): ?>		
					<?php if($options->get('back_home_btn_label' ) ): ?>
                    <div class="btns-box">
                        <a class="btn-one" href="<?php echo( home_url( '/' ) ); ?>">
                            <span class="txt"><?php echo wp_kses( $options->get( 'back_home_btn_label'), $allowed_html ); ?></span>
                        </a>
                    </div>
					<?php else: ?>
					<div class="btns-box">
                        <a class="btn-one" href="<?php echo( home_url( '/' ) ); ?>">
                            <span class="txt"><?php esc_html_e( 'Back to Home', 'roofplan' ); ?></span>
                        </a>
                    </div>
					<?php endif; ?>		
					<?php endif; ?>
					
                </div>    
            </div>
        </div>       
    </div>
</section> 
	
  
<?php
}
get_footer(); ?>
