<?php


/** Set ABSPATH for execution */
define( 'ABSPATH', dirname(dirname(__FILE__)) . '/' );
define( 'WPINC', 'wp-includes' );


/**
 * @ignore
 */
function add_filter() {}

/**
 * @ignore
 */
function esc_attr($str) {return $str;}

/**
 * @ignore
 */
function apply_filters() {}

/**
 * @ignore
 */
function get_option() {}

/**
 * @ignore
 */
function is_lighttpd_before_150() {}

/**
 * @ignore
 */
function add_action() {}

/**
 * @ignore
 */
function did_action() {}

/**
 * @ignore
 */
function do_action_ref_array() {}

/**
 * @ignore
 */
function get_bloginfo() {}

/**
 * @ignore
 */
function is_admin() {return true;}

/**
 * @ignore
 */
function site_url() {}

/**
 * @ignore
 */
function admin_url() {}

/**
 * @ignore
 */
function home_url() {}

/**
 * @ignore
 */
function includes_url() {}

/**
 * @ignore
 */
function wp_guess_url() {}

if ( ! function_exists( 'json_encode' ) ) :
/**
 * @ignore
 */
function json_encode() {}
endif;



/* Convert hexdec color string to rgb(a) string */
 
function hex2rgba($color, $opacity = false) {
 
	$default = 'rgb(0,0,0)';
 
	//Return default if no color provided
	if(empty($color))
          return $default; 
 
	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}

$color = $_GET['main_color'];

ob_start(); ?>
/*========== Color ====================*/


.service-home-four .content-box .list li:before,
.sec-title .sub-title p,
.feature-block-one .inner-box .icon-box .icon,
.feature-block-one .inner-box .lower-content .light-icon,
.main-menu .navigation>li:hover>a, .main-menu .navigation>li.current>a,
.main-header.home_four .header-contact-info ul li .icon span:before,
.header-contact-info ul li .text h6 a:hover,
.serach-button-style1 .search-toggler:hover,
.main-header.home_four .shopping-cart-box a:hover,
.funfact-home-4 .single-item .icon-box,
.chooseus-home-four .single-item .icon-box,
.project-home-four .project-block .inner-box .zoom-button a,
.project-home-four .project-block .inner-box .text h4 a:hover,
.team-home-four .team-block .inner-box .share-box:hover .share-icon,
.team-home-four .team-block .inner-box .share-box .share-links li a:hover,
.accordion-box .block .acc-btn.active h3,
.accordion-box .block .acc-btn h3 b,
.accordion-box .block .acc-btn .icon-outer,
.workig-process-home-four .process-block .inner-box:hover .icon-box .icon,
.single-blog-style1 .text-holder .meta-info li a:hover,
.single-blog-style1 .text-holder .blog-title a:hover,
.footer-widget-contact-info ul li .inner .icon,
.footer-widget-contact-info ul li .inner .text p a:hover,
.footer-widget-links ul li a:hover,
.subscribe-form input[type="email"]:focus + button, .subscribe-form button:hover,
.footer-bottom .bottom-inner .copyright p a,
.shopping-cart-box a,
.single-service-style1 .title-holder .icon span::before,
.single-service-style1 .title-holder h3 a:hover,
.single-service-style1 .title-holder .readmore-button a,
.single-project-item .img-holder .zoom-button a,
.single-project-item .img-holder .overlay-content .inner h4 a:hover,
.single-team-style1:hover .title-holder .share-icon a,
.main-menu .navigation>li:hover>a, .main-menu .navigation>li.current>a,
.shopping-cart-box a,
.choose-style1-content-box .inner-content ul li .icon span::before,
.main-header.home_four .header-top__right .support-box .icon,
.about-style2-counter .single-fact-counter .icon span::before,
.team-details-content .content-box .designation,
.team-details-content .content-box .info li i,
.team-details-content .content-box .info li a:hover,
.team-details .education-box .single-item span,
.single-price-box .top h4,
.single-price-box .price-list ul li span::before,
.single-price-box .table-footer .btn-one:hover,
.error-content .big-title,
.single-features-style1 .icon span::before,
.single-sidebar .service-page-link li.active a,
.owl-nav-style-one.owl-theme .owl-nav [class*="owl-"]:before,
.single-sidebar .service-page-link li a:hover,
.project-details-text-box3 .text ul li span::before,
.project-details-text-box4 .text ul li span::before,
.breadcrumb-menu ul li:hover a, .breadcrumb-menu ul li.active,
.product-block-two .price,
.woocommerce .woocommerce-info a,
.styled-pagination li a i,
.single-blog-style1:hover .text-holder .readmore-button a,
.mrwidget ul li:hover a,
.sidebar-blog-post ul li .title-box h4 a:hover,
blockquote p:before,
.single-contact-info-box .icon span::before

{
	color:#<?php echo esc_attr( $color ); ?>!important;
}

/*==============================================
   Theme Background Css
===============================================*/

.service-home-four .tab-btn:hover, 
.service-home-four .tab-btn.active-btn,
.service-home-four .tab-btn:before,
.main-header.home_four .nav-btn:hover .bar,
.btn-one:after,
.search-popup .search-form fieldset input[type="submit"],
.main-slider.home-4 .content .upper-title h5:before,
.btn-one.style2:before,
.funfact-home-4 .single-item .icon-box:before,
.chooseus-home-four .single-item .overlay-text,
.owl-carousel.owl-dot-style1 .owl-dots .owl-dot.active,
.project-home-four .project-block .inner-box .zoom-button a:hover,
.accordion-box .block .acc-btn.active .icon-outer,
.faq-style1-area.home-four .image-box .bg-layer,
.workig-process-home-four .process-block .inner-box .icon-box .icon,
.single-blog-style1 .text-holder .meta-info li::before,
.contact-style1-area,
.footer-widget-links ul li a:before,
.subscribe-form button,
.footer-social-link ul li a:before,
.main-menu .navigation> li> ul:before, .main-menu .navigation> li> .megamenu:before,
.main-menu .navigation> li> ul> li> ul:before, .main-menu .navigation> li> ul> li> .megamenu:before,
.single-features-style1 .icon .shape1::after,
.single-service-style1 .img-holder .inner:before,
.single-service-style1 .title-holder .icon .shape1::after,
.single-service-style1 .title-holder .readmore-button a:hover,
.single-team-style1 .title-holder:before,
.single-team-style1 .title-holder .share-icon a,
.team-social-link li a:hover,
.loader-wrap .layer .overlay,
.main-slider .owl-theme .owl-nav .owl-prev:hover, .main-slider .owl-theme .owl-nav .owl-next:hover,
.contact-style3-area,
.main-slider .banner-carousel.owl-carousel button.owl-dot.active,
.about-style2__image1 .red-box,
.contact-style2__title,
.team-details-content .content-box .social-links li a:hover,
.team-details .progress-box .bar-inner,
.single-price-box::before,
.service-details-text3 .single-box .counting .inner,
.owl-nav-style-one.owl-theme .owl-nav [class*="owl-"]:hover,
.woocommerce #place_order, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button,
.woocommerce nav.woocommerce-pagination ul li span,
.pagination li .page-numbers.current,
.styled-pagination li:hover a, .styled-pagination li.active a,
.wp-block-search .wp-block-search__button,
.widget_tag_cloud a:hover,
.post-social-link ul li a:hover

{
	background: #<?php echo esc_attr( $color ); ?>!important;
	background-color:#<?php echo esc_attr( $color ); ?>!important;
}


/*==============================================
   Theme Border Color Css
===============================================*/

.about-style3-content-box ul li:nth-child(1) .single-counting-box.style2,
.shopping-cart-box a,
.single-counting-box,
.team-details-content .content-box .social-links li a:hover,
.woocommerce nav.woocommerce-pagination ul li span,
.pagination li .page-numbers.current,
.styled-pagination li:hover a, .styled-pagination li.active a,
.widget_tag_cloud a:hover,
.post-social-link ul li a:hover

{
    border-color: #<?php echo esc_attr( $color ); ?>!important;  
}

/*==============================================
   RGB
===============================================*/

.video-galler-outer-bg:before {
    background-color: <?php echo esc_attr( hex2rgba($color, 0.9));?>!important;
}
.main-slider .content h3:before{
    background: <?php echo esc_attr( hex2rgba($color, 0.4));?>!important;
}




<?php 

$out = ob_get_clean();
$expires_offset = 31536000; // 1 year
header('Content-Type: text/css; charset=UTF-8');
header('Expires: ' . gmdate( "D, d M Y H:i:s", time() + $expires_offset ) . ' GMT');
header("Cache-Control: public, max-age=$expires_offset");
header('Vary: Accept-Encoding'); // Handle proxies
header('Content-Encoding: gzip');

echo gzencode($out);
exit;