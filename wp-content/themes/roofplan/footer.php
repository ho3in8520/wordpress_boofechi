<?php
$options = roofplan_WSH()->option();
$allowed_html = wp_kses_allowed_html( 'post' );

$image_logo2 = $options->get( 'image_normal_logo2' );
$logo_dimension2 = $options->get( 'normal_logo_dimension2' );

$logo_type = '';
$logo_text = '';
$logo_typography = '';
/**
 * Footer Main File.
 *
 * @package ROOFPLAN
 * @author  template_path
 * @version 1.0
 */
global $wp_query;
$page_id = ( $wp_query->is_posts_page ) ? $wp_query->queried_object->ID : get_the_ID();
$options = roofplan_WSH()->option();
?>



<?php roofplan_template_load( 'templates/footer/footer.php', compact( 'page_id' ) );?>



<?php if(!$options->get( 'to_top' ) ):?>
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="icon-right-arrow-1"></span>
	</button> 
<?php endif; ?>

<div id="search-popup" class="search-popup">
    <div class="close-search"><i class="icon-close"></i></div>
    <div class="popup-inner">
        <div class="overlay-layer"></div>
        <div class="search-form">
            <form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <div class="form-group">
                    <fieldset>
                        <input type="search" class="form-control" name="s" value="" placeholder="<?php echo wp_kses( $options->get( 'search_text_v1'), $allowed_html ); ?>" required >
                        <input type="submit" value="Search Now!" class="theme-btn style-four">
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
</div>
</main>
<?php wp_footer(); ?>
</body>
</html>
