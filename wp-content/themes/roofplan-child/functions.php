<?php
/**
 * Theme functions and definitions.
 */
function roofplan_child_enqueue_styles() {

    if ( SCRIPT_DEBUG ) {
        wp_enqueue_style( 'roofplan-style' , get_template_directory_uri() . '/style.css' );
    } else {
        wp_enqueue_style( 'roofplan-minified-style' , get_template_directory_uri() . '/style.min.css' );
    }

    wp_enqueue_style( 'roofplan-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( 'roofplan-style' ),
        wp_get_theme()->get('Version')
    );
}

add_action(  'wp_enqueue_scripts', 'roofplan_child_enqueue_styles' );